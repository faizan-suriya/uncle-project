export const Assets = {
  mainBackground: require("./sh_bg.png"),
  splashBackground: require("./splash.png"),
  logo: require("./sh_logo.png"),
  frontImage: require("./front.png"),
  backImage: require("./back.png"),
  cardPlaceholder: require("./card_placeholder.png"),
  hikingImage: require("./hiking-images.jpeg"),
  surfingImage: require("./surfing.jpeg"),
  onlineCourses: require("./Udemy-Courses.jpg"),
  onlineExperiments: require("./experiments.jpg"),
  roots: require("./roots.png"),
  postImage: require("./forumplaceholder.png"),
  universityIcon: require("./universityicon.png"),
  ssuetLogo: require("./ssuet.png")
};
