import React, { Component } from "react";
import { Image, StyleSheet, ScrollView } from "react-native";
import {
  Container,
  Content,
  View,
  Button,
  Left,
  Right,
  Icon,
  Card,
  CardItem,
  Thumbnail,
  Body,
  Text
} from "native-base";

import SlideshowGallery from "../../components/slideshow";
import { Tabs } from "../../routes";
import TabViewExample from "../../components/tabview";
import Navbar from "../../components/navbar";
import SideMenuDrawer from "../../components/sidemenudrawer";
import { Assets } from "../../../assets";
import { Colors } from "../../styles/colors";

export default class PickerWithIcon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      names: [
        { name: "Limited Time Offer", id: 1 },
        { name: "Fashion", id: 2 },
        { name: "Science", id: 3 },
        { name: "Art", id: 4 },
        { name: "Mathematics", id: 5 }
      ]
    };
  }
  // static navigationOptions = {
  //   title: "Home"
  // };
  render() {
    var left = (
      <Left>
        <Button onPress={() => this._sideMenuDrawer.open()} transparent>
          <Icon name="ios-menu" style={{ color: Colors.black }} />
        </Button>
      </Left>
    );
    var right = (
      <Right>
        <Button onPress={() => Actions.search()} transparent>
          <Icon name="ios-search" style={{ color: Colors.black }} />
        </Button>
        <Button onPress={() => Actions.cart()} transparent>
          <Icon
            name="ios-notifications"
            style={{ color: Colors.black }}
          />
        </Button>
      </Right>
    );
    return (
      <SideMenuDrawer ref={ref => (this._sideMenuDrawer = ref)}>
        <Container>
          <Content>
            <Navbar left={left} right={right} title="Explore" />
            <View>
              <ScrollView
                horizontal={true}
                contentContainerStyle={{
                  flexDirection: "row"
                }}
              >
                {this.state.names.map((item, index) => (
                  <View key={item.id} style={styles.item}>
                    <Text
                      style={{
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: "bold"
                      }}
                    >
                      {item.name}
                    </Text>
                  </View>
                ))}
              </ScrollView>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: "column",
                padding: 10
                // backgroundColor: "green"
              }}
            >
              <View style={{ backgroundColor: "red" }}>
                <SlideshowGallery />
              </View>
              <View
                style={{
                  flex: 1,
                  // backgroundColor: "grey",
                  flexDirection: "row",
                  paddingTop: 10,
                  paddingBottom: 5
                  // backgroundColor  : "orange"
                }}
              >
                <View style={{ flex: 1 }}>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    All Limited Time Only
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "flex-end"
                  }}
                >
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    View all
                  </Text>
                  {/* <Icon name="ios-arrow-forward" style={{fontSize: 12}}/> */}
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row"
                }}
              >
                <View style={{ flex: 1, padding: 3 }}>
                  <Card transparent>
                    <CardItem cardBody>
                      <Image
                        source={Assets.surfingImage}
                        style={{
                          height: 100,
                          width: null,
                          flex: 1,
                          borderTopLeftRadius: 20,
                          borderTopRightRadius: 20
                        }}
                      />
                    </CardItem>
                    <CardItem
                      style={{
                        height: 50,
                        backgroundColor: Colors.cardColor,
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20
                      }}
                    >
                      <Body
                        style={{
                          alignItems: "center",
                          justifyContent: "center",
                          flexDirection: "column"
                        }}
                      >
                        <View
                          style={{
                            backgroundColor: Colors.limitedDiscount,
                            paddingTop: 5,
                            paddingBottom: 5,
                            paddingLeft: 10,
                            paddingRight: 10,
                            position: "absolute",
                            top: -25
                          }}
                        >
                          <Text
                            style={{
                              color: Colors.black,
                              fontSize: 10,
                              fontWeight: "bold"
                            }}
                          >
                            Limited Time Only!
                          </Text>
                        </View>
                        <View
                          style={{
                            paddingTop: 20,
                            paddingBottom: 10,
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                        >
                          <Text
                            style={{
                              color: Colors.white,
                              fontWeight: "bold",
                              fontSize: 14
                            }}
                          >
                            Surfing
                          </Text>
                          <Text
                            style={{
                              color: Colors.discountPercent,
                              fontSize: 12,
                              fontWeight: "bold"
                            }}
                          >
                            25 % Off
                          </Text>
                        </View>
                      </Body>
                    </CardItem>
                  </Card>
                </View>
                <View style={{ flex: 1, padding: 3 }}>
                  <Card transparent>
                    <CardItem cardBody>
                      <Image
                        source={Assets.hikingImage}
                        style={{
                          height: 100,
                          width: null,
                          flex: 1,
                          borderTopLeftRadius: 20,
                          borderTopRightRadius: 20
                        }}
                      />
                    </CardItem>
                    <CardItem
                      style={{
                        height: 50,
                        backgroundColor: Colors.cardColor,
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20
                      }}
                    >
                      <Body
                        style={{
                          alignItems: "center",
                          justifyContent: "center",
                          flexDirection: "column"
                        }}
                      >
                        <View
                          style={{
                            backgroundColor: Colors.limitedDiscount,
                            paddingTop: 5,
                            paddingBottom: 5,
                            paddingLeft: 10,
                            paddingRight: 10,
                            position: "absolute",
                            top: -25
                          }}
                        >
                          <Text
                            style={{
                              color: Colors.black,
                              fontSize: 10,
                              fontWeight: "bold"
                            }}
                          >
                            Limited Time Offer!
                          </Text>
                        </View>
                        <View
                          style={{
                            paddingTop: 20,
                            paddingBottom: 10,
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                        >
                          <Text
                            style={{
                              color: Colors.white,
                              fontWeight: "bold",
                              fontSize: 14
                            }}
                          >
                            Hiking
                          </Text>
                          <Text
                            style={{
                              color: Colors.discountPercent,
                              fontSize: 12,
                              fontWeight: "bold"
                            }}
                          >
                            25 % Off
                          </Text>
                        </View>
                      </Body>
                    </CardItem>
                  </Card>
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row"
                }}
              >
                <View style={{ flex: 1, padding: 3 }}>
                  <Card transparent>
                    <CardItem cardBody>
                      <Image
                        source={Assets.onlineCourses}
                        style={{
                          height: 100,
                          width: null,
                          flex: 1,
                          borderTopLeftRadius: 20,
                          borderTopRightRadius: 20
                        }}
                      />
                    </CardItem>
                    <CardItem
                      style={{
                        height: 60,
                        backgroundColor: Colors.cardColor,
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20
                      }}
                    >
                      <Body
                        style={{
                          alignItems: "center",
                          justifyContent: "center",
                          flexDirection: "column"
                        }}
                      >
                        <View
                          style={{
                            backgroundColor: Colors.limitedDiscount,
                            paddingTop: 5,
                            paddingBottom: 5,
                            paddingLeft: 10,
                            paddingRight: 10,
                            position: "absolute",
                            top: -25
                          }}
                        >
                          <Text
                            style={{
                              color: Colors.black,
                              fontSize: 10,
                              fontWeight: "bold"
                            }}
                          >
                            Limited Time Offer!
                          </Text>
                        </View>
                        <View
                          style={{
                            paddingTop: 20,
                            paddingBottom: 10,
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                        >
                          <Text
                            style={{
                              color: Colors.white,
                              fontWeight: "bold",
                              fontSize: 14
                            }}
                          >
                            Online Courses
                          </Text>
                          <Text
                            style={{
                              color: Colors.discountPercent,
                              fontSize: 12,
                              fontWeight: "bold"
                            }}
                          >
                            25 % Off
                          </Text>
                        </View>
                      </Body>
                    </CardItem>
                  </Card>
                </View>
                <View style={{ flex: 1, padding: 3 }}>
                  <Card transparent>
                    <CardItem cardBody>
                      <Image
                        source={Assets.onlineExperiments}
                        style={{
                          height: 100,
                          width: null,
                          flex: 1,
                          borderTopLeftRadius: 20,
                          borderTopRightRadius: 20
                        }}
                      />
                    </CardItem>
                    <CardItem
                      style={{
                        height: 60,
                        backgroundColor: Colors.cardColor,
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20
                      }}
                    >
                      <Body
                        style={{
                          alignItems: "center",
                          justifyContent: "center",
                          flexDirection: "column"
                        }}
                      >
                        <View
                          style={{
                            backgroundColor: Colors.limitedDiscount,
                            paddingTop: 5,
                            paddingBottom: 5,
                            paddingLeft: 10,
                            paddingRight: 10,
                            position: "absolute",
                            top: -25
                          }}
                        >
                          <Text
                            style={{
                              color: Colors.black,
                              fontSize: 10,
                              fontWeight: "bold"
                            }}
                          >
                            Limited Time Offer!
                          </Text>
                        </View>
                        <View
                          style={{
                            paddingTop: 20,
                            paddingBottom: 10,
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                        >
                          <Text
                            style={{
                              color: Colors.white,
                              fontWeight: "bold",
                              fontSize: 14
                            }}
                          >
                            Online Experiments
                          </Text>
                          <Text
                            style={{
                              color: Colors.discountPercent,
                              fontSize: 12,
                              fontWeight: "bold"
                            }}
                          >
                            25 % Off
                          </Text>
                        </View>
                      </Body>
                    </CardItem>
                  </Card>
                </View>
              </View>
            </View>
          </Content>
        </Container>
      </SideMenuDrawer>
      // <SideMenuDrawer>
      // <Container>
      //   <Navbar left={left} right={right} title="HOME" />
      // <View style={styles.maincontainer}>
      //   <View style={styles.slidercontainer}>
      //     <SlideshowGallery />
      //   </View>
      //   <View style={styles.tabscontainer}>
      //     <TabViewExample {...this.props} />
      //   </View>
      // </View>
      // </Container>
      // </SideMenuDrawer>
    );
  }
}

const styles = StyleSheet.create({
  maincontainer: {
    flex: 1
  },

  item: {
    // flexDirection: "row",
    // width: 150,
    justifyContent: "space-between",
    alignItems: "center",
    // margin: 2,
    padding: 10,
    // borderColor: "#2a4944",
    // borderWidth: 1,
    backgroundColor: Colors.tabsBackground
  },
  slidercontainer: {},
  // cardscontainer: {
  //   flex: 1,
  //   flexDirection
  // },
  icon: {
    width: 24,
    height: 24
  }
});
