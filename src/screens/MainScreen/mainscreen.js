import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  Button,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  Dimensions
} from "react-native";
import {
  createDrawerNavigator,
  DrawerItems,
  createStackNavigator
} from "react-navigation";
import HomeScreen from "../HomeScreen/homescreen";
import ListItem from "../../components/listitem";
import Feedback from "../FeedbackScreen/feedbackscreen";
import DealsScreen from "../DealsScreen";
import StoreInfo from "../InfoScreen";
import InviteFriends from "../InviteFriends";
import RewardsScreen from "../RewardsScreen";

export default class Main extends Component {
  render() {
    return (
      <View>
        <Text>This is menu drawer</Text>
      </View>
    );
    // return <AppDrawerNavigator />;
  }
}

const CustomDrawerComponent = props => (
  <SafeAreaView style={{ flex: 1 }}>
    <View
      style={{
        height: 150,
        backgroundColor: "white",
        alignItems: "center",
        justifyContent: "center"
      }}
    >
      <Image
        source={require("../../../assets/user.png")}
        style={{ height: 120, width: 120 }}
      />
    </View>
    <ScrollView>
      <DrawerItems {...props} />
    </ScrollView>
  </SafeAreaView>
);

const HomeStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen
    },
    listItemScreen: {
      screen: ListItem
    }
  },
  {
    headerMode: "none"
  }
);

const AppDrawerNavigator = createDrawerNavigator(
  {
    Home: HomeStack,
    list: ListItem,
    settingScreen: Feedback,
    InviteFriends: InviteFriends,
    RewardsScreen: RewardsScreen
  },

  {
    contentComponent: CustomDrawerComponent
  }
);
