import React from "react";
import { TouchableOpacity, FlatList } from "react-native";
import {
  View,
  Container,
  Content,
  left,
  right,
  Card,
  CardItem,
  Body,
  Text,
  Button,
  Left,
  Icon
} from "native-base";
import { Colors } from "../../styles/colors";
import services from "../../services";
import ImageLoad from "react-native-image-placeholder";

import Navbar from "../../components/navbar";

const VendorItem = ({ item, onSelectItem }) => (
  <View style={{ flex: 1, padding: 8 }}>
    <Card transparent>
      <TouchableOpacity onPress={() => onSelectItem(item.item)}>
        <CardItem cardBody>
          <ImageLoad
            style={{
              height: 90,
              width: "90%",
              flex: 1
              //  borderTopLeftRadius: 50,
              //  borderTopRightRadius: 50
            }}
            loadingStyle={{ size: "large", color: "gray" }}
            source={{ uri: item.item.picture }}
          />
        </CardItem>
        <CardItem
          style={{
            height: 50,
            backgroundColor: Colors.cardColor,
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20
          }}
        >
          <Body
            style={{
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column"
            }}
          >
            <View
              style={{
                backgroundColor: Colors.limitedDiscount,
                paddingTop: 5,
                paddingBottom: 5,
                paddingLeft: 10,
                paddingRight: 10,
                position: "absolute",
                top: -25
              }}
            >
              <Text //Fashion
                style={{
                  color: Colors.white,
                  fontSize: 10,
                  fontWeight: "bold"
                }}
              >
                Favourite
              </Text>
            </View>
            <View //Pizza Hut
              style={{
                paddingTop: 20,
                paddingBottom: 10,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  color: Colors.maroonColor,
                  fontWeight: "bold",
                  fontSize: 13
                }}
              >
                {item.item.name}
              </Text>
              <Text //25% OFF
                style={{
                  color: Colors.discountPercent,
                  fontSize: 11,
                  fontWeight: "bold"
                }}
              >
                25 % Off
              </Text>
            </View>
          </Body>
        </CardItem>
      </TouchableOpacity>
    </Card>
  </View>
);

class Favourite extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vendors: [],
      category: "5bdad917914e1109ce2cb164"
    };
  }
  componentDidMount() {
    this.getVendorsByCategory();
  }

  async getVendorsByCategory(category) {
    try {
      const getData = await services.auth.getVendorsByCategory(
        this.state.category
      );
      this.setState({ vendors: getData.data });
    } catch (error) {
      // (error);
    }
  }

  async onSelectItem(item, index) {
    this.props.navigation.navigate("RevealCodeScreen", { vendor: item });
  }

  render() {
    // const category = this.props.navigation.state.params.category;
    // // ("This is the category id", category.name);
    var left = (
      <Left>
        <Button onPress={() => this.props.navigation.pop()} transparent>
          <Icon name="ios-arrow-back" style={{ color: Colors.white }} />
        </Button>
      </Left>
    );

    return (
      <Container>
        <Content>
          <Navbar left={left} right={right} title="Favourites" />

          <FlatList
            style={{ flex: 1 }}
            numColumns={2}
            data={this.state.vendors}
            keyExtractor={(item, index) => item.id}
            renderItem={(item, index) => (
              <VendorItem
                item={item}
                onSelectItem={this.onSelectItem.bind(this)}
              />
            )}
          />
        </Content>
      </Container>
    );
  }
}
export default Favourite;
