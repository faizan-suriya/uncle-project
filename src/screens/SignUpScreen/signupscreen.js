import React from "react";
import {
  Container,
  View,
  Left,
  Right,
  Button,
  Icon,
  Item,
  Input,
  Text
} from "native-base";
import { StyleSheet, TouchableOpacity, Linking } from "react-native";
import Snackbar from "react-native-snackbar";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import * as userUtil from "../../utils/user.util";
// import TextInputMask from "react-native-text-input-mask";
import MainBackground from "../../components/MainBackground";
import Logo from "../../components/logo";
import {
  nameConstraint,
  emailConstraint,
  phoneConstraint,
  passwordConstraint
} from "../../utils/constraints";
var validate = require("validate.js");
import { String } from "../../utils/strings";
import { Colors } from "../../styles/colors";
import services, { updateHeaders } from "../../services";
import * as authUtil from "../../utils/auth.util";

class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastname: "",
      email: "",
      mobile: "",
      password: "",
      confirmPassword: ""
    };
  }
  _verify = async () => {
    var constraints = {
      firstName: nameConstraint,
      lastName: nameConstraint,
      email: emailConstraint,
      phone: phoneConstraint,
      password: passwordConstraint
    };
    var validation = validate(
      {
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        email: this.state.email,
        phone: this.state.mobile,
        password: this.state.password
      },
      constraints,
      { format: "flat" }
    );
    //   // (validation);
    if (validation) {
      Snackbar.show({
        title: validation[0],
        duration: Snackbar.LENGTH_LONG
      });
      return;
    }

    try {
      if (this.state.password !== this.state.confirmPassword) {
        Snackbar.show({
          title: "Password do not match",
          duration: Snackbar.LENGTH_LONG
        });
        return;
      }
      // ("This is the state", this.state);
      const { data } = await services.auth.registerUser(this.state);
      // (data);
      updateHeaders(data.token);
      await authUtil.setToken(data.token);
      await userUtil.setUserInfo(data);
      this.props.navigation.navigate("verificationScreen");
    } catch (error) {
      Snackbar.show({
        title: error.response.data.message,
        duration: Snackbar.LENGTH_LONG
      });
      // (error.response);
    }
  };
  render() {
    return (
      <MainBackground>
        <KeyboardAwareScrollView>
          <Container style={{ backgroundColor: "transparent" }}>
            {/* <Navbar left={left} right={right} title="LOGIN" /> */}
            <View style={styles.mainContainer}>
              <View style={styles.topcontainer}>
                <View style={styles.logocontainer}>
                  <Logo />
                </View>
                <View style={{ marginBottom: 15 }}>
                  <Text style={styles.mainTitle}>Create Account,</Text>
                  <Text style={styles.subTitle}>Signup to continue</Text>
                </View>
              </View>

              <View style={styles.bottomcontainer}>
                <Item>
                  <Icon active name="ios-person" style={styles.icons} />
                  <Input
                    style={styles.input}
                    placeholder={String.firstName}
                    onChangeText={text => this.setState({ firstName: text })}
                    placeholderTextColor={Colors.placeholderColor}
                  />
                </Item>
                <Item>
                  <Icon active name="ios-person" style={styles.icons} />
                  <Input
                    style={styles.input}
                    placeholder={String.lastName}
                    onChangeText={text => this.setState({ lastName: text })}
                    placeholderTextColor={Colors.placeholderColor}
                  />
                </Item>
                <Item>
                  <Icon active name="ios-mail" style={styles.icons} />
                  <Input
                    style={styles.input}
                    placeholder={String.email}
                    onChangeText={text => this.setState({ email: text.trim() })}
                    placeholderTextColor={Colors.placeholderColor}
                    autoCapitalize="none"
                    keyboardType="email-address"
                  />
                </Item>
                <Item>
                  <Icon active name="ios-call" style={styles.icons} />
                  <Input
                    style={styles.input}
                    placeholder={String.phone}
                    placeholderTextColor={Colors.placeholderColor}
                    keyboardType="phone-pad"
                    refInput={ref => {
                      this.input = ref;
                    }}
                    onChangeText={text =>
                      this.setState({ mobile: text.trim() })
                    }
                  />
                </Item>
                <Item>
                  <Icon active name="ios-lock" style={styles.icons} />
                  <Input
                    style={styles.input}
                    placeholder={String.password}
                    onChangeText={text => this.setState({ password: text })}
                    secureTextEntry={true}
                    placeholderTextColor={Colors.placeholderColor}
                  />
                </Item>
                <Item>
                  <Icon active name="ios-lock" style={styles.icons} />
                  <Input
                    style={styles.input}
                    placeholder={String.confirmPassword}
                    onChangeText={text =>
                      this.setState({ confirmPassword: text })
                    }
                    secureTextEntry={true}
                    placeholderTextColor={Colors.placeholderColor}
                  />
                </Item>

                <View
                  style={{
                    alignSelf: "stretch"
                  }}
                >
                  <TouchableOpacity
                    onPress={this._verify}
                    style={styles.button}
                  >
                    <Text style={styles.buttonText} uppercase={false}>
                      {String.signup}
                    </Text>
                  </TouchableOpacity>
                  <View style={{ alignItems: "center", marginTop: 5 }}>
                    <Text
                      style={styles.textstyle}
                      onPress={() => Linking.openURL("https://www.google.com")}
                    >
                      By signing up, I agree to the terms and conditions
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </Container>
        </KeyboardAwareScrollView>
      </MainBackground>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: "center"
    // alignItems: "center",
  },

  topcontainer: {
    justifyContent: "center",
    paddingLeft: 30,
    paddingRight: 30,
    marginTop: 20
  },

  bottomcontainer: {
    flex: 1,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    paddingTop: 5,
    paddingLeft: 30,
    paddingRight: 30,
    //marginTop:5,
    // marginBottom:20,
    backgroundColor: Colors.formBackground
  },
  mainTitle: {
    fontSize: 26,
    fontWeight: "normal",
    textAlign: "left",
    width: "100%",
    color: Colors.title
  },

  subTitle: {
    fontSize: 18,
    textAlign: "left",
    width: "100%",
    color: Colors.subTitle
  },

  smallcontent: {
    fontSize: 16,
    textAlign: "left",
    width: "100%",
    color: Colors.content
  },
  icons: {
    color: Colors.white
  },

  input: {
    fontSize: 14,
    color: Colors.white
  },

  button: {
    marginTop: 12,
    alignSelf: "stretch",
    height: 45,
    borderRadius: 10,
    backgroundColor: Colors.buttonBackground,
    alignItems: "center",
    justifyContent: "center"
  },

  buttonText: {
    color: Colors.buttonText,
    fontWeight: "bold",
    fontSize: 16
  },

  linktext: {
    alignSelf: "stretch",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15
  },

  textstyle: {
    marginTop: 5,
    textAlign: "center",
    color: Colors.white,
    fontWeight: "normal",
    fontSize: 13
  },

  logocontainer: {
    marginBottom: 30,
    marginTop: 10
  }
});

export default Signup;
