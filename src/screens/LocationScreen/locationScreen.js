import React, { Component } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import Map from "../../components/map";

export default class LocationScreen extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "flex-end",
          alignItems: "center"
        }}
      >
        <Map />
      </View>
    );
  }
}
