import React, { Component } from "react";
import { View, Text, Image, StyleSheet } from "react-native";

export default class Feedback extends Component {
  static navigationOptions = {
    drawerLabel: "Home",
    drawerIcon: ({ tintColor }) => (
      <Image
        source={require("../../../assets/drawer.png")}
        style={[styles.icon, { tintColor: tintColor }]}
      />
    )
  };
  render() {
    return (
      <View>
        <Text>This is Feedback Screen</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  icon: {
    width: 24,
    height: 24
  }
});
