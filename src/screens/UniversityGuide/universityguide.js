import React, { Component } from "react";
import { View, Text, Left, Right, Icon, Button } from "native-base";
import {
  StyleSheet,
  TextInput,
  FlatList,
  TouchableOpacity
} from "react-native";
import Icons from "react-native-vector-icons/Ionicons";
import { SearchBar } from "react-native-elements";
import { Colors } from "../../styles/colors";
import Navbar from "../../components/navbar";
import services from "../../services";

class UniversityGuide extends Component {
  constructor() {
    super();
    this.state = {
      universities: []
    };
  }

  _onSelectItem = item => {
    this.props.navigation.navigate("guideDetail", { info: item });
  };

  renderSeparator = () => (
    <View
      style={{
        backgroundColor: "#d3d3d3",
        height: 1
      }}
    />
  );

  componentDidMount() {
    this._getUniversities();
  }

  _getUniversities = async () => {
    const getData = await services.auth.getAllUniversities();
    // // ("This is the university", getData);
    this.setState({ universities: getData.data });
    // ("This is the state", this.state.universities);
  };

  render() {
    var left = (
      <Left>
        <Button onPress={() => this.props.navigation.pop()} transparent>
          <Icon name="ios-arrow-back" style={{ color: Colors.white }} />
        </Button>
      </Left>
    );
    var right = (
      <Right>
        <Button onPress={() => alert("This is the notification")} transparent>
          <Icon name="ios-notifications" style={{ color: Colors.black }} />
        </Button>
      </Right>
    );
    return (
      <View style={{ backgroundColor: "white", flex: 1 }}>
        <Navbar left={left} title="University Guide" />
        <View>
          <SearchBar
            lightTheme
            searchIcon={{ size: 15, marginLeft: 125 }}
            placeholder="Search"
            platform="default"
            inputContainerStyle={{ backgroundColor: "white" }}
            inputStyle={{ fontSize: 13 }}
            containerStyle={{ backgroundColor: "#BBBCB6", borderRadius: 5 }}
          />
        </View>

        <View style={{ flex: 1 }}>
          <FlatList
            data={this.state.universities}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() => {
                  this._onSelectItem(item);
                }}
              >
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1 }}>
                    <Text style={styles.item}> {item.name} </Text>
                  </View>

                  <View>
                    <Icons
                      name="ios-arrow-forward"
                      size={18}
                      color="#a9a9a9"
                      style={{ marginRight: 15, marginTop: 7 }}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            )}
            ItemSeparatorComponent={this.renderSeparator}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  searchBar: {
    marginTop: 8,
    marginHorizontal: 10,
    marginBottom: 8,
    fontSize: 12,
    color: "#d3d3d3",
    borderRadius: 8,
    flex: 1,
    backgroundColor: "white",
    textAlign: "center"
  },

  item: {
    padding: 5,
    fontSize: 14,
    height: 40
  }
});

export default UniversityGuide;
