
import React from "react";
import {Alert, StyleSheet,FlatList, KeyboardAvoidingView, Dimensions, Image, TouchableOpacity} from "react-native";
import {
  Container,
  Content,
  View,
  Left,
  Right,
  Icon,
  Button,
  Card,
  CardItem,
  Thumbnail,
  Body,
  Text
} from "native-base";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { StackActions, NavigationActions,Seperator,Row } from "react-navigation";
import Navbar from "../../components/navbar";
import { Colors } from "../../styles/colors";
import Share from "react-native-share";


const { width, height } = Dimensions.get("window");

export default class About extends React.Component {


    shareBtnPressed=()=>{

    }
    earnBtnPressed=()=>{
    }

    shopBtnPressed=()=>{

    }
    startBtnPressed=()=>{
    	const shareOptions = {
	      title: "StudentHub",
	      message: "Hey, I've got some awesome discounts on StudentHub App, Download it to avail discounts on famous brands"
	    };
	    return Share.open(shareOptions);
    }

    static navigationOptions = ({ navigation }) => ({
        title: "Rewards"
      });
    render() {
    	var left = (
	      <Left>
	        <Button onPress={() => this.props.navigation.pop()} transparent>
	          <Icon name="ios-arrow-back" style={{ color: Colors.white }} />
	        </Button>
	      </Left>
	    );
      return (
         <Container style={StyleSheet.absoluteFill}>
         	<Navbar left={left} title="Invite Friends" />
		        <Text style={styles.headingText}>
		              Invite friends and earn Rewards
		         </Text>
		         <View style={styles.shareButton}>

		         <Button onPress={this.shareBtnPressed} 
		             style={styles.shareButton}>
		              <Text style={{color: Colors.maroonColor}}>Share</Text>
		             </Button>
		         </View>


		          <Text style={styles.shareText}>
		              Friends join StudentHub using your personal referal links. When they join,Your account is automatically credited.
		         </Text>

		         <View style={styles.shareButton}>
		         <Button onPress = {() => this.earnBtnPressed()} 
		             style={styles.shareButton}>
		             <Text style={{color: Colors.maroonColor}}>Earn</Text>
		             </Button>
		         </View>
		    
		         <Text style={styles.shareText}>
		             The more friends you invite the more you learn, You will get Rs. 100 Credit for each friends that joins.
		         </Text>


		         <View style={styles.shareButton}>
		         <Button onPress={this.shopBtnPressed} 
		             style={styles.shareButton}>
		             <Text style={{color: Colors.maroonColor}}>Shop</Text>
		             </Button>
		         </View>
		    
		         <Text style={styles.shareText}>
		            When you've earned enough rewards, use it for subscription
		         </Text>

		         <View style={styles.shareButton}>
		         <Button onPress={this.shareBtnPressed} 
		             style={styles.shareButton}>
		             <Text style={{fontWeight: "bold", color: Colors.maroonColor, flex: 1, textAlign: "center"}} > Terms & Conditions</Text>
	             </Button>
		         </View>

		    

		      	{/* <View style={styles.startButton}>

		         <Button onPress={this.startBtnPressed} >
		             style={{alignSelf: "center"}}
		             <Text> Share Now</Text>
		             </Button>
				 </View> */}
				 
				 <TouchableOpacity onPress={this._login} style={styles.button}>
                    <Text style={styles.buttonText} uppercase={false}>
					Share Now
                    </Text>
                  </TouchableOpacity>





        </Container>
  
      );
    }
  }
  const styles = StyleSheet.create({


	startButton: {
	    width: '100%',
	    justifyContent: 'center',
	    alignItems: 'center',
	    position: 'absolute',
	    bottom:60,
	    left:0
	 },

	shareText: {
	    fontSize: 14.0,
	    fontWeight: 'normal',
	    marginLeft:15,
	    marginRight:15,
	    marginTop:5,
	    textAlign: 'center',
	    color:'black',
	    lineHeight: 20
	  },

	shareButton: {
	    marginTop:4,
	    justifyContent: 'center',
	    alignItems: 'center',
	    alignSelf: 'center',
	    backgroundColor: "transparent"
	 },
    headingText: {
        fontSize: 18.0,
        fontWeight: 'bold',
        marginLeft:20,
        marginTop:20,
        textAlign: 'center',
        color:'black',
	  },
	  button: {
		marginTop: 20,
		width: 250,
		height: 45,
		borderRadius: 10,
		backgroundColor: Colors.buttonText,
		alignItems: "center",
		justifyContent: "center",
		alignSelf: 'center',
	  },
	
	  buttonText: {
		color: Colors.buttonBackground,
		fontWeight: "bold",
		fontSize: 16
	  }
  });