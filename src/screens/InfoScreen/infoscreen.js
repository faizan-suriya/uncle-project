import React, { Component } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import StoreInfo from "../../components/info";

export default class InfoScreen extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1
        }}
      >
        <StoreInfo />
      </View>
    );
  }
}
