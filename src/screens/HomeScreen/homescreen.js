import React, { Component } from "react";
import {
  TouchableOpacity,
  Image,
  StyleSheet,
  ScrollView,
  FlatList,
  Platform
} from "react-native";
import {
  Container,
  Content,
  View,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Icon,
  Thumbnail,
  Body,
  Text,
  Input
} from "native-base";
import Icons from "react-native-vector-icons/Ionicons";
import SlideshowGallery from "../../components/slideshow";
import Navbar from "../../components/navbar";
import SideMenuDrawer from "../../components/sidemenudrawer";
import { Assets } from "../../../assets";
import { Colors } from "../../styles/colors";
import services from "../../services";
import ImageLoad from "react-native-image-placeholder";
import { EventRegister } from "react-native-event-listeners";
import authServices from "../../services/users";

const VendorItem = ({ item, onSelectItem, index, category }) => (
  <View style={{ flex: 1, padding: 8 }}>
    <Card transparent>
      <TouchableOpacity onPress={() => onSelectItem(item.item)}>
        <CardItem cardBody>
          <ImageLoad
            style={{
              height: 90,
              width: "90%",
              flex: 1
              //  borderTopLeftRadius: 50,
              //  borderTopRightRadius: 50
            }}
            loadingStyle={{ size: "large", color: "gray" }}
            source={{ uri: item.item.picture }}
          />

          {/* <Image
            style={{
              height: 90,
              width: "90%",
              flex: 1,
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20
            }}
            placeholderSource={Assets.User}
            // borderRadius={10}
            loadingStyle={{ size: "large", color: "gray" }}
            source={{ uri: item.item.picture }}
          /> */}
        </CardItem>
        <CardItem
          style={{
            height: 50,
            backgroundColor: Colors.cardColor,
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20
          }}
        >
          <Body
            style={{
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column"
            }}
          >
            <View
              style={{
                backgroundColor: Colors.limitedDiscount,
                paddingTop: 5,
                paddingBottom: 5,
                paddingLeft: 10,
                paddingRight: 10,
                position: "absolute",
                top: -25
              }}
            >
              <Text //Fashion
                style={{
                  color: Colors.white,
                  fontSize: 10,
                  fontWeight: "bold"
                }}
              >
                {category}
              </Text>
            </View>
            <View
              style={{
                paddingTop: 20,
                paddingBottom: 10,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  color: Colors.maroonColor,
                  fontWeight: "bold",
                  fontSize: 13
                }}
              >
                {item.item.name}
              </Text>
              <Text
                style={{
                  color: Colors.discountPercent,
                  fontSize: 11,
                  fontWeight: "bold"
                }}
              >
                {item.item.discount + " % Off"}
              </Text>
            </View>
          </Body>
        </CardItem>
      </TouchableOpacity>
    </Card>
  </View>
);

const SearchBar = ({ placeholder }) => (
  <View
    style={{
      flexDirection: "row",
      width: 200,
      alignItems: "center",
      height: 40,
      marginTop: Platform.OS == "android" ? 25 : null
    }}
  >
    <Input
      style={{
        alignItems: "center",
        justifyContent: "center",
        color: Colors.maroonColor,
        width: 200,
        height: 40
      }}
      autoCapitalize="none"
      placeholder="Search Here..."
      placeholderTextColor="gray"
      onEndEditing={event => {
        EventRegister.emit("searchTapped", event.nativeEvent.text);
      }}
    />
    {/* <TouchableOpacity onPress={() => console.log("")}>
      <Icon name="ios-close-circle" size={22} color="white" />
    </TouchableOpacity> */}
  </View>
);

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      vendors: [],
      premium: [],
      selectedIndex: 0
    };
  }

  componentDidMount() {
    this.categories();
    this.getPremiumVendors();
    // loader.show();
    // const categories = await authUtil.getCategories();
    // const { navigation } = this.props;
    // const categories = navigation.getPaaram('categories');
    // // ("This is store categories", categories);
    // this.setState({categories : categories});

    this.listener = EventRegister.addEventListener(
      "searchTapped",
      async text => {
        try {
          if (text) {
            const response = await authServices.search({ name: text });
            console.log(response);
            this.setState({ vendors: response.data });
          }
        } catch (error) {
          alert(error);
        }
      }
    );
  }

  _viewAll = selectedCategory => {
    // ("This is view all id", selectedCategory);
    this.props.navigation.navigate("ViewComponent", {
      category: selectedCategory
    });
  };

  async categories() {
    try {
      const getData = await services.auth.getCategories();
      // // ("This is the Categories", getData);
      await this.setState({
        categories: getData.data,
        selectedCategory: getData.data[0]
      });
      this.getVendorsByCategory(getData.data[0]._id);
    } catch (error) {
      // (error);
    }
  }

  async getVendorsByCategory(categoryId) {
    try {
      const getData = await services.auth.getVendorsByCategory(categoryId);
      this.setState({ vendors: getData.data });
    } catch (error) {
      // (error);
    }
  }

  async getPremiumVendors() {
    try {
      const getData = await services.auth.getPremiumVendors();
      this.setState({ premium: getData.data });
    } catch (error) {
      // (error);
    }
  }

  async onSelectItem(item, index) {
    this.props.navigation.navigate("RevealCodeScreen", { vendor: item });
  }
  // static navigationOptions = {
  //   title: "Home"
  // };
  render() {
    var left = (
      <Left>
        <Button onPress={() => this._sideMenuDrawer.open()} transparent>
          <Icons name="ios-menu" size={23} style={{ color: Colors.white }} />
        </Button>
      </Left>
    );
    var right = (
      <Right>
        {/* <TouchableOpacity
          // style={{ backgroundColor: Colors.white, borderRadius:20, padding:8 }}
          onPress={() => alert("This is the Search")} */}

        <Button onPress={() => alert("This is the Search")} transparent>
          <Icons name="md-search" size={23} style={{ color: Colors.black }} />
        </Button>
        {/* </TouchableOpacity> */}
      </Right>
    );
    return (
      <SideMenuDrawer ref={ref => (this._sideMenuDrawer = ref)} {...this.props}>
        <Container>
          <Content>
            <Navbar left={left} right={right} title="Explore" />
            <View>
              <SearchBar />
              <ScrollView
                // scrollIndicatorInsets={{white}
                indicatorStyle="white"
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{
                  flexDirection: "row"
                }}
              >
                {this.state.categories.map((item, index) => (
                  <TouchableOpacity
                    activeOpacity={1}
                    key={item._id}
                    style={[
                      styles.item,
                      {
                        borderBottomWidth:
                          this.state.selectedIndex == index ? 2 : 0
                      }
                    ]}
                    onPress={() => {
                      this.setState({
                        selectedCategory: item,
                        selectedIndex: index
                      });
                      this.getVendorsByCategory(item._id);
                    }}
                  >
                    <Text
                      style={{
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: "bold"
                      }}
                    >
                      {item.name}
                    </Text>
                  </TouchableOpacity>
                ))}
              </ScrollView>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: "column",
                padding: 10,
                paddingBottom: 40,
                backgroundColor: Colors.formBackground,
                borderBottomRightRadius: 30,
                borderBottomLeftRadius: 30
              }}
            >
              {this.state.premium.length > 0 ? (
                <View style={{ backgroundColor: "black" }}>
                  <SlideshowGallery data={this.state.premium} />
                </View>
              ) : null}
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                padding: 10
                // backgroundColor: "red"
                // borderRadius:20
              }}
            >
              <View style={{ flex: 1 }}>
                <Text style={{ fontSize: 16, fontWeight: "bold" }}>
                  {(this.state.selectedCategory || {}).name || ""}
                </Text>
              </View>

              <TouchableOpacity
                onPress={() => {
                  this._viewAll(this.state.selectedCategory);
                }}
                style={{ flexDirection: "row" }}
              >
                <Text
                  style={{
                    color: Colors.black,
                    marginRight: 10,
                    fontWeight: "bold"
                  }}
                >
                  View all
                </Text>
                <Icons
                  name="ios-arrow-forward"
                  style={{ color: Colors.black }}
                  size={22}
                />
              </TouchableOpacity>
            </View>

            <FlatList
              style={{ flex: 1 }}
              numColumns={2}
              data={this.state.vendors}
              keyExtractor={(item, index) => item.id}
              renderItem={(item, index) => (
                <VendorItem
                  key={item.item._id}
                  item={item}
                  onSelectItem={this.onSelectItem.bind(this)}
                  index={index}
                  category={this.state.selectedCategory.name}
                />
              )}
            />
          </Content>
        </Container>
      </SideMenuDrawer>
      // <SideMenuDrawer>
      // <Container>
      //   <Navbar left={left} right={right} title="HOME" />
      // <View style={styles.maincontainer}>
      //   <View style={styles.slidercontainer}>
      //     <SlideshowGallery />
      //   </View>
      //   <View style={styles.tabscontainer}>
      //     <TabViewExample {...this.props} />
      //   </View>
      // </View>
      // </Container>
      // </SideMenuDrawer>
    );
  }
}

const styles = StyleSheet.create({
  maincontainer: {
    flex: 1
  },

  item: {
    // flexDirection: "row",
    // width: 150,
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomColor: "yellow",
    // margin: 2,
    padding: 10,
    // borderColor: "#2a4944",
    // borderWidth: 1,
    backgroundColor: Colors.formBackground
  },
  slidercontainer: {},
  // cardscontainer: {
  //   flex: 1,
  //   flexDirection
  // },
  icon: {
    width: 24,
    height: 24
  }
});
