import React from "react";
import {
  Container,
  Content,
  View,
  Left,
  Right,
  Button,
  Icon,
  Item,
  Input,
  Text,
  Thumbnail
} from "native-base";
import {
  StyleSheet,
  TextInput,
  AsyncStorage,
  TouchableOpacity,
  ScrollView,
  Platform,
  Image
} from "react-native";
import Logo from "../../components/logo";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import Snackbar from "react-native-snackbar";
import MainBackground from "../../components/MainBackground";
import services, { updateHeaders } from "../../services";
import * as authUtil from "../../utils/auth.util";
import * as userUtil from "../../utils/user.util";
import { get } from "lodash";
import { emailConstraint, passwordConstraint } from "../../utils/constraints";
var validate = require("validate.js");
import { String } from "../../utils/strings";
import { Colors } from "../../styles/colors";
import { StackActions, NavigationActions } from "react-navigation";
import { Assets } from "../../../assets";
import Icons from "react-native-vector-icons/Ionicons";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      hidePassword: true
    };
  }

  managePasswordVisibility = () => {
    this.setState({ hidePassword: !this.state.hidePassword });
  };

  async componentDidMount() {
    const loginStatus = await authUtil.isLoggedIn();
    const token = await authUtil.getToken();
    updateHeaders(token);
    // ("LoginToken>>>>", loginStatus);
    if (loginStatus) {
      this.props.navigation.dispatch(
        StackActions.reset({
          index: 0,
          key: null,
          actions: [NavigationActions.navigate({ routeName: "Home" })]
        })
      );
    }
    // AsyncStorage.clear();
  }

  _createAccount = () => {
    this.props.navigation.navigate("signupScreen");
  };

  _forgetPassword = () => {
    this.props.navigation.navigate("forgetPassword");
  };

  _resetToScreen = routeName => {
    this.props.navigation.dispatch(
      NavigationActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName })]
      })
    );
  };

  _login = async () => {
    var constraints = {
      email: emailConstraint,
      password: passwordConstraint
    };
    var validation = validate(
      { email: this.state.email, password: this.state.password },
      constraints,
      { format: "flat" }
    );
    //   // (validation);
    if (validation) {
      Snackbar.show({
        title: validation[0],
        duration: Snackbar.LENGTH_LONG
      });
      // alert(validation[0]);
      return;
    }
    try {
      console.log("These are the states", this.state);
      const { data } = await services.auth.login(this.state);
      console.log("Login data", data);
      updateHeaders(data.token);
      await authUtil.setToken(data.token);
      await userUtil.setUserInfo(data);
      if (data.isVerified) {
        if (data.institute) {
          if (data.activated) {
            setTimeout(() => {
              this.props.navigation.dispatch(
                StackActions.reset({
                  index: 0,
                  key: null,
                  actions: [NavigationActions.navigate({ routeName: "Home" })]
                })
              );
            }, 2000);
          } else {
            authUtil.logout();
            // Snackbar.show({
            //   title: "Your Profile is not activated Yet",
            //   duration: Snackbar.LENGTH_LONG
            // });
            this.props.navigation.navigate("ActivationMessage");
          }
        } else {
          setTimeout(() => {
            this.props.navigation.dispatch(
              StackActions.reset({
                index: 0,
                key: null,
                actions: [
                  NavigationActions.navigate({
                    routeName: "instituteScreen"
                  })
                ]
              })
            );
          }, 2000);
        }
      } else {
        this.props.navigation.navigate("verificationScreen");
      }
    } catch (error) {
      console.log(error);
      Snackbar.show({
        title: error.response.data.message,
        duration: Snackbar.LENGTH_LONG
      });
      // (error.response);
    }
  };
  render() {
    return (
      <MainBackground>
        <KeyboardAwareScrollView>
          <Container style={{ backgroundColor: "transparent" }}>
            {/* <Navbar left={left} right={right} title="LOGIN" /> */}
            <View style={styles.mainContainer}>
              <View style={styles.topcontainer}>
                <View style={styles.logocontainer}>
                  <Logo />
                </View>

                <View style={{ marginBottom: 15 }}>
                  <Text style={styles.mainTitle}>Welcome, </Text>
                  <Text style={styles.subTitle}>Sign in to continue </Text>
                </View>
                <View
                  style={{
                    borderBottomColor: Colors.separator,
                    borderBottomWidth: 3,
                    width: "15%"
                  }}
                />
                <View style={{ marginTop: 15, marginBottom: 20 }}>
                  <Text style={styles.smallcontent}>
                    For the best experience{"\n"}with Student Hub
                  </Text>
                </View>
              </View>

              <View style={styles.bottomcontainer}>
                <View style={{ marginBottom: 10 }}>
                  <Item>
                    <Icon
                      active
                      name="ios-mail"
                      size={25}
                      style={styles.icons}
                    />
                    <Input
                      style={styles.input}
                      placeholder={String.email}
                      onChangeText={text =>
                        this.setState({ email: text.trim() })
                      }
                      placeholderTextColor={Colors.placeholderColor}
                      autoCapitalize="none"
                      keyboardType="email-address"
                    />
                  </Item>
                </View>
                {/* <View style={{ marginBottom: 10 }}>
                  <Item>
                    <Icon active name="ios-lock" style={styles.icons} />
                    <Input
                      style={styles.input}
                      placeholder={String.password}
                      onChangeText={text => this.setState({ password: text })}
                      secureTextEntry={true}
                      placeholderTextColor={Colors.placeholderColor}
                    />
                  </Item>
                </View> */}

                <View style={{ marginBottom: 10 }}>
                  <Item>
                    <Icon
                      name="ios-lock"
                      size={25}
                      color="white"
                      flexDirection="column"
                      style={styles.icons}
                    />

                    <Input
                      placeholder={String.password}
                      onChangeText={text => this.setState({ password: text })}
                      placeholderTextColor={Colors.placeholderColor}
                      secureTextEntry={this.state.hidePassword}
                      style={styles.input}
                    />

                    <TouchableOpacity
                      activeOpacity={0.8}
                      style={styles.visibilityBtn}
                      onPress={this.managePasswordVisibility}
                    >
                      {this.state.hidePassword ? (
                        <Icon
                          name="ios-eye-off"
                          size={25}
                          color={Colors.white}
                          flexDirection="column"
                          style={styles.icons}
                        />
                      ) : (
                        <Icon
                          name="ios-eye"
                          size={25}
                          color={Colors.white}
                          flexDirection="column"
                          style={styles.icons}
                        />
                      )}
                    </TouchableOpacity>
                  </Item>
                </View>

                <View
                  style={{
                    alignSelf: "stretch"
                  }}
                >
                  <TouchableOpacity onPress={this._login} style={styles.button}>
                    <Text style={styles.buttonText} uppercase={false}>
                      {String.login}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={this._createAccount}
                    style={styles.button}
                  >
                    <Text style={styles.buttonText} uppercase={false}>
                      {String.signup}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.linktext}
                    onPress={this._forgetPassword}
                  >
                    <Text style={styles.textstyle} uppercase={false}>
                      {String.forgetPassword}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Container>
        </KeyboardAwareScrollView>
      </MainBackground>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: "center"
    // alignItems: "center",
  },

  topcontainer: {
    justifyContent: "center",
    paddingLeft: 30,
    paddingRight: 30,
    marginTop: 20
  },

  bottomcontainer: {
    flex: 1,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    paddingTop: 25,
    paddingLeft: 30,
    paddingRight: 30,
    backgroundColor: Colors.formBackground
  },
  mainTitle: {
    fontSize: 26,
    fontWeight: "normal",
    textAlign: "left",
    width: "100%",
    color: Colors.title
  },

  subTitle: {
    fontSize: 18,
    textAlign: "left",
    width: "100%",
    color: Colors.subTitle
  },

  smallcontent: {
    fontSize: 16,
    textAlign: "left",
    width: "100%",
    color: Colors.content
  },

  icons: {
    color: Colors.white
  },

  input: {
    color: Colors.white
    // paddingRight: 45,
    // paddingLeft: 18,
    // paddingVertical: 0
  },
  logocontainer: {
    marginBottom: 30,
    marginTop: 10
  },

  button: {
    marginTop: 20,
    alignSelf: "stretch",
    height: 45,
    borderRadius: 10,
    backgroundColor: Colors.buttonBackground,
    alignItems: "center",
    justifyContent: "center"
  },

  buttonText: {
    color: Colors.buttonText,
    fontWeight: "bold",
    fontSize: 16
  },

  linktext: {
    alignSelf: "stretch",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15
  },

  textstyle: {
    color: Colors.white,
    fontWeight: "bold",
    fontSize: 16
  },

  container2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 25,
    paddingTop: Platform.OS === "ios" ? 20 : 0
  },

  textBoxBtnHolder: {
    // flex:1,
    position: "relative",
    alignSelf: "stretch",
    justifyContent: "center"
    //flexDirection: 'column'
  },

  textBox: {
    //color:"white",
    fontSize: 18,
    alignSelf: "stretch",
    height: 45,
    paddingRight: 45,
    paddingLeft: 16,
    // borderWidth: 1,
    paddingVertical: 0
    // borderColor: "grey",
    // borderRadius: 5
  },

  visibilityBtn: {
    position: "absolute",
    right: 3,
    height: 40,
    width: 35,
    padding: 5
  },

  btnImage: {
    resizeMode: "contain",
    height: "100%",
    width: "100%",
    tintColor: "white"
    //color:"black"
  }
});

export default Login;
