import React, { Component } from "react";
import { StyleSheet, TouchableOpacity, ScrollView } from "react-native";
import {
  Container,
  View,
  Text,
  Content,
  Left,
  Right,
  Item,
  Icon,
  Input,
  Button
} from "native-base";
import { Colors } from "../../styles/colors";
import Navbar from "../../components/navbar";
import { String } from "../../utils/strings";
import TabViewDetail from "../../components/Info_details";

class UpdateProfile extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    var left = (
      <Left>
        <Button onPress={() => this.props.navigation.pop()} transparent>
          <Icon name="ios-arrow-back" style={{ color: Colors.white }} />
        </Button>
      </Left>
    );

    return (
      <Container style={{ flex: 1, backgroundColor: "transparent" }}>
        <Navbar left={left} title="UpdateProfile" />

        <View style={{ flex: 1, margin: 10 }}>
          <TabViewDetail />
        </View>
      </Container>
    );
  }
}

export default UpdateProfile;
