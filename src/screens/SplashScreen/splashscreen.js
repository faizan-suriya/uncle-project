import React, { Component } from "react";
import {
  Container,
  View,
  Spinner,
  Left,
  Right,
  Button,
  Icon,
  Item,
  Input,
  Text,
  Thumbnail
} from "native-base";
import { AsyncStorage, StyleSheet, Platform } from "react-native";
import services, { updateHeaders } from "../../services";
import { Colors } from "../../styles/colors";
import * as authUtil from "../../utils/auth.util";
import * as userUtil from "../../utils/user.util";
import { NavigationActions, StackActions } from "react-navigation";
import { Assets } from "../../../assets";

class Splash extends Component {
  async componentDidMount() {
    const login = await authUtil.isLoggedIn();
    const token = await authUtil.getToken();
    const user = await userUtil.getUserInfo();
    updateHeaders(token);
    console.log("This is the splash login", login);
    console.log("This is the splash token", token);
    console.log("This is the splash user info", user);
    if (login) {
      if (user.isVerified) {
        if (user.institute) {
          if (user.activated) {
            setTimeout(() => {
              this.props.navigation.dispatch(
                StackActions.reset({
                  index: 0,
                  key: null,
                  actions: [NavigationActions.navigate({ routeName: "Home" })]
                })
              );
            }, 2000);
          } else {
            setTimeout(() => {
              this.props.navigation.dispatch(
                StackActions.reset({
                  index: 0,
                  key: null,
                  actions: [
                    NavigationActions.navigate({
                      routeName: "ActivationMessage"
                    })
                  ]
                })
              );
            }, 2000);
          }
        } else {
          setTimeout(() => {
            this.props.navigation.dispatch(
              StackActions.reset({
                index: 0,
                key: null,
                actions: [
                  NavigationActions.navigate({
                    routeName: "instituteScreen"
                  })
                ]
              })
            );
          }, 2000);
        }
      } else {
        setTimeout(() => {
          this.props.navigation.dispatch(
            StackActions.reset({
              index: 0,
              key: null,
              actions: [
                NavigationActions.navigate({ routeName: "verificationScreen" })
              ]
            })
          );
        }, 2000);
      }
    } else {
      setTimeout(() => {
        this.props.navigation.dispatch(
          StackActions.reset({
            index: 0,
            key: null,
            actions: [NavigationActions.navigate({ routeName: "loginScreen" })]
          })
        );
      }, 2000);
    }
  }

  render() {
    return (
      <Container style={{ backgroundColor: "#421B2C" }}>
        {/* <Navbar left={left} right={right} title="LOGIN" /> */}
        <View style={styles.mainContainer}>
          <Thumbnail
            square
            style={{
              width: 180,
              height: 88
            }}
            resizeMode="contain"
            source={Assets.logo}
          />
          <Text style={styles.mainTitle}>
            For the Students, by the Students
          </Text>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  topcontainer: {
    justifyContent: "center",
    paddingLeft: 30,
    paddingRight: 30,
    marginTop: 20
  },

  bottomcontainer: {
    flex: 1,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    paddingTop: 25,
    paddingLeft: 30,
    paddingRight: 30,
    backgroundColor: Colors.maroonColor
  },
  mainTitle: {
    marginTop: 15,
    fontSize: 20,
    fontWeight: "normal",
    color: Colors.white
  },

  subTitle: {
    fontSize: 20,
    fontWeight: "normal",
    color: Colors.white
  },

  smallcontent: {
    fontSize: 24,
    width: "100%",
    color: Colors.content
  },

  icons: {
    color: Colors.white
  },

  input: {
    color: Colors.white
    // paddingRight: 45,
    // paddingLeft: 18,
    // paddingVertical: 0
  },
  logocontainer: {
    marginBottom: 30,
    marginTop: 10
  },

  button: {
    marginTop: 20,
    alignSelf: "stretch",
    height: 45,
    borderRadius: 10,
    backgroundColor: Colors.title,
    alignItems: "center",
    justifyContent: "center"
  },

  buttonText: {
    color: Colors.white,
    fontWeight: "bold",
    fontSize: 16
  },

  linktext: {
    alignSelf: "stretch",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15
  },

  textstyle: {
    color: Colors.white,
    fontWeight: "bold",
    fontSize: 16
  },

  container2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 25,
    paddingTop: Platform.OS === "ios" ? 20 : 0
  },

  textBoxBtnHolder: {
    // flex:1,
    position: "relative",
    alignSelf: "stretch",
    justifyContent: "center"
    //flexDirection: 'column'
  },

  textBox: {
    //color:"white",
    fontSize: 18,
    alignSelf: "stretch",
    height: 45,
    paddingRight: 45,
    paddingLeft: 16,
    // borderWidth: 1,
    paddingVertical: 0
    // borderColor: "grey",
    // borderRadius: 5
  },

  visibilityBtn: {
    position: "absolute",
    right: 3,
    height: 40,
    width: 35,
    padding: 5
  },

  btnImage: {
    resizeMode: "contain",
    height: "100%",
    width: "100%",
    tintColor: "white"
    //color:"black"
  }
});

export default Splash;
