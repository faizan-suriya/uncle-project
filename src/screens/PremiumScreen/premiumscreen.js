import React, { Component } from "react";
import Navbar from "../../components/navbar";
import {
  Container,
  Content,
  View,
  Button,
  Left,
  Right,
  Icon,
  Header,
  Footer,
  Card,
  CardItem,
  Thumbnail,
  CheckBox,
  Body,
  Text
} from "native-base";
import { Colors } from "../../styles/colors";
import { StyleSheet, TouchableOpacity, Image } from "react-native";
import { Assets } from "../../../assets";
import { String } from "../../utils/strings";
import Icons from "react-native-vector-icons/Entypo";

class Premium extends Component {
  render() {
    var left = (
      <Left>
        <Button onPress={() => this.props.navigation.pop()} transparent>
          <Icon name="ios-arrow-back" style={{ color: Colors.white }} />
        </Button>
      </Left>
    );

    return (
      <Container>
        <Navbar left={left} title="Subscribe" />
        <View style={styles.container}>
          <Card style={{ flex: 1 }}>
            <CardItem header>
              <Thumbnail
                square
                style={{
                  height: 200,
                  width: "100%"
                }}
                placeholderSource={Assets.User}
                loadingStyle={{ size: "large", color: "gray" }}
                source={Assets.hikingImage}
              />
            </CardItem>
            <CardItem>
              <Body>
                <View
                  style={{
                    marginTop: -10,
                    justifyContent: "center",
                    alignItems: "center",
                    alignSelf: "center"
                  }}
                >
                  <Text style={{ fontSize: 35, fontWeight: "bold" }}>
                    RS100
                  </Text>
                  <Text>Per Month</Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 25
                  }}
                >
                  <Icons
                    name="dot-single"
                    size={30}
                    style={{ color: Colors.discountPercent }}
                  />

                  <Text style={{ color: "gray", marginLeft: 15 }}>
                    Unlimited Storage
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 5
                  }}
                >
                  <Icons
                    name="dot-single"
                    size={30}
                    style={{ color: Colors.discountPercent }}
                  />
                  <Text style={{ color: "gray", marginLeft: 15 }}>
                    Daily Newletter
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 5
                  }}
                >
                  <Icons
                    name="dot-single"
                    size={30}
                    style={{ color: Colors.discountPercent }}
                  />
                  <Text style={{ color: "gray", marginLeft: 15 }}>
                    16 Free Course
                  </Text>
                </View>
                <View
                  style={{
                    alignSelf: "center"
                  }}
                >
                  <TouchableOpacity onPress={this._login} style={styles.button}>
                    <Text style={styles.buttonText} uppercase={false}>
                      {String.subscribe.toUpperCase()}
                    </Text>
                  </TouchableOpacity>
                </View>
              </Body>
            </CardItem>
          </Card>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15
  },
  itemBlock: {
    flexDirection: "row",
    paddingBottom: 5,
    flex: 1
  },
  itemImage: {
    width: 80,
    height: 80,
    borderRadius: 20
  },
  itemMeta: {
    marginLeft: 10,
    justifyContent: "center",
    flex: 1
  },
  itemName: {
    fontSize: 16,
    fontWeight: "bold",
    color: Colors.cardColor
  },
  itemLastMessage: {
    fontSize: 12,
    color: Colors.black
  },
  separator: {
    height: 0.5,
    width: "80%",
    alignSelf: "center",
    backgroundColor: "#555"
  },
  header: {
    padding: 10
  },
  headerText: {
    fontSize: 30,
    fontWeight: "900"
  },
  button: {
    marginTop: 20,
    width: 250,
    height: 45,
    borderRadius: 10,
    backgroundColor: Colors.buttonText,
    alignItems: "center",
    justifyContent: "center"
  },

  buttonText: {
    color: Colors.buttonBackground,
    fontWeight: "bold",
    fontSize: 16
  }
});

export default Premium;
