import React, { Component } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import DealListView from "../../components/deallistview";
export default class DealsScreen extends Component {
  render() {
    return (
      <View>
        <DealListView />
      </View>
    );
  }
}
