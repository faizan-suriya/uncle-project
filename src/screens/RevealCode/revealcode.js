import React from "react";
import {
  Container,
  View,
  Left,
  Right,
  Button,
  Icon,
  Item,
  Input,
  Text,
  Thumbnail
} from "native-base";
import {
  StyleSheet,
  AsyncStorage,
  TouchableOpacity,
  Linking,
  Clipboard
} from "react-native";
import Icons from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import Logo from "../../components/logo";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import Snackbar from "react-native-snackbar";
import MainBackground from "../../components/MainBackground";
import services from "../../services";
import * as authUtil from "../../utils/auth.util";
import { get } from "lodash";
import { emailConstraint, passwordConstraint } from "../../utils/constraints";
var validate = require("validate.js");
import { String } from "../../utils/strings";
import { Colors } from "../../styles/colors";
import { Assets } from "../../../assets";
import Navbar from "../../components/navbar";
import ImageLoad from "react-native-image-placeholder";
import authServices from "../../services/users";

class RevealCode extends React.Component {
  constructor() {
    super();
    this.state = {
      favorite: false
    };
  }

  onClickNotFavourite = () => {
    this.setState({ favorite: false });
  };

  onClickFavourite = () => {
    Snackbar.show({
      title: " Added to favourites",
      duration: Snackbar.LENGTH_SHORT
    });
    this.setState({ favorite: true });
  };

  async revealCodePressed() {
    try {
      const getData = await services.auth.revealCode({
        vendor: this.props.navigation.state.params.vendor._id
      });
      // // ("This is the Categories", getData);
      this.setState({ code: getData.data });
    } catch (error) {
      // (error);
    }
  }

  render() {
    var left = (
      <Left>
        <Button onPress={() => this.props.navigation.pop()} transparent>
          <Icon name="ios-arrow-back" style={{ color: Colors.white }} />
        </Button>
      </Left>
    );
    var right = this.state.favorite ? (
      <Right>
        <Button onPress={this.onClickNotFavourite} transparent>
          <MaterialIcon
            name="favorite"
            size={23}
            style={{ color: Colors.white }}
          />
        </Button>
      </Right>
    ) : (
      <Right>
        <Button onPress={this.onClickFavourite} transparent>
          <MaterialIcon
            name="favorite-border"
            size={23}
            style={{ color: Colors.red }}
          />
        </Button>
      </Right>
    );
    return (
      <Container style={{ opacity: 0.9 }}>
        <Navbar left={left} right={right} title="Reveal Code" />
        <View style={styles.mainContainer}>
          <View style={styles.logocontainer}>
            <ImageLoad
              style={styles.brandImage}
              borderRadius={10}
              loadingStyle={{ size: "large", color: "gray" }}
              source={{
                uri: this.props.navigation.state.params.vendor.picture
              }}
            />
          </View>
          <View style={{ marginBottom: 10, alignItems: "center" }}>
            <Text style={styles.mainTitle}>
              {this.props.navigation.state.params.vendor.name}
            </Text>
            <TouchableOpacity
              style={{ flexDirection: "row", justifyContent: "center" }}
              onPress={async () => {
                try {
                  authServices.updateHitCount({
                    vendor: this.props.navigation.state.params.vendor._id
                  });
                } catch (error) {}

                Linking.openURL(
                  this.props.navigation.state.params.vendor.website ||
                    "http://www.google.com"
                );
              }}
            >
              <Icons name="web" size={14} color={Colors.black} />
              <Text style={{ fontSize: 14, marginLeft: 5 }}>
                {this.props.navigation.state.params.vendor.website ||
                  "http://www.google.com"}
              </Text>
            </TouchableOpacity>
          </View>

          {/* <View style={{ marginBottom: 10 }}>
            <Text
              style={{
                fontSize: 16,
                fontWeight: "bold",
                color: Colors.darkGrey
              }}
            >
              Valid from 12/8/2018 to 10/9/2018
            </Text>
          </View> */}
          <View style={{ marginBottom: 15, alignItems: "center" }}>
            <Text
              style={{
                fontSize: 22,
                fontWeight: "bold",
                color: Colors.black
              }}
            >
              {this.props.navigation.state.params.vendor.discount + "% Off"}
            </Text>
            <Text
              style={{
                fontSize: 13,
                color: "#00003f"
              }}
            >
              {this.props.navigation.state.params.vendor.description}
            </Text>
          </View>
          <View style={{ marginBottom: 15 }}>
            <Text
              style={{
                fontSize: 14,
                fontWeight: "normal",
                color: Colors.black
              }}
            >
              Reveal your code and show it at the store
            </Text>
          </View>

          <View
            style={{
              alignSelf: "center",
              marginBottom: 4
            }}
          >
            <Button
              disabled={this.state.code != undefined}
              onPress={() => this.revealCodePressed()}
              style={styles.button}
            >
              <Text style={styles.buttonText}>
                {this.state.code ? this.state.code.code : String.revealCode}
              </Text>
            </Button>
            {this.state.code ? (
              <Button
                onPress={() => Clipboard.setString(this.state.code.code)}
                style={styles.button}
              >
                <Text style={styles.buttonText}>Copy</Text>
              </Button>
            ) : null}
          </View>

          <View style={{ marginTop: 10, alignItems: "center" }}>
            <TouchableOpacity
              onPress={() => Linking.openURL("http://google.com")}
            >
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: "bold",
                  color: Colors.darkGrey,
                  textAlign: "center"
                }}
              >
                <Text style={{ color: Colors.black }}>Student Hub</Text>
                {"\n"}
                For the Students, by the Students
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 5, marginBottom: 5, alignItems: "center" }}>
            <Button full onPress={this._createAccount} style={styles.button}>
              <Text style={styles.buttonText}>{String.support}</Text>
            </Button>
          </View>
          <View style={{ marginBottom: 15, flexDirection: "row" }}>
            <TouchableOpacity
              onPress={() => Linking.openURL("http://google.com")}
            >
              <Icons
                name="facebook"
                size={30}
                color={Colors.black}
                style={styles.icons}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => Linking.openURL("http://google.com")}
            >
              <Icons
                name="twitter"
                size={30}
                color={Colors.black}
                style={styles.icons}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => Linking.openURL("http://google.com")}
            >
              <Icons
                name="instagram"
                size={30}
                color={Colors.black}
                style={styles.icons}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => Linking.openURL("http://google.com")}
            >
              <Icons
                name="whatsapp"
                size={30}
                color={Colors.black}
                style={styles.icons}
              />
            </TouchableOpacity>
          </View>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 30,
    paddingRight: 30
  },

  brandImage: {
    width: 153,
    height: 113,
    borderRadius: 10
  },
  mainTitle: {
    fontSize: 16,
    fontWeight: "bold",
    width: "100%",
    color: Colors.maroonColor
  },

  subTitle: {
    fontSize: 18,
    textAlign: "left",
    width: "100%",
    color: Colors.white
  },

  icons: {
    padding: 5
  },

  input: {
    color: Colors.white
  },
  logocontainer: {
    marginBottom: 15,
    alignItems: "center"
  },

  button: {
    width: 200,
    justifyContent: "center",
    backgroundColor: Colors.buttonText,
    marginTop: 20
  },

  buttonText: {
    color: Colors.buttonBackground
  }
});

export default RevealCode;
