import React from "react";
import {
  createStackNavigator,
  createMaterialTopTabNavigator
} from "react-navigation";

import Splash from "../screens/SplashScreen";
import Login from "../screens/LoginScreen";
import SignUp from "../screens/SignUpScreen";
import Verification from "../screens/VerificationScreen";
import PickerWithIcon from "../screens/CheckingScreen";
import RevealCode from "../screens/RevealCode";
import ForgetPassword from "../screens/ForgetPassword";
import Main from "../screens/MainScreen/mainscreen";
import FoodScreen from "../screens/FoodScreen";
import RetailScreen from "../screens/RetailScreen";
import BeautyScreen from "../screens/BeautyScreen";
import EntertaimentScreen from "../screens/EntertaimentScreen";
import listitem from "../components/listitem";
import DealsScreen from "../screens/DealsScreen";
import InfoScreen from "../screens/InfoScreen";
import LocationScreen from "../screens/LocationScreen";
import DiscussionForum from "../screens/DiscussionForum";
import Institute from "../screens/InstituteSreen";
import HomeScreen from "../screens/HomeScreen";
import InviteFriends from "../screens/InviteFriends";
import TopicPost from "../screens/TopicPost";
import AddForum from "../screens/AddForum";
import Home from "../screens/HomeScreen";
import { TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import ListItem from "../components/listitem";
import Explore from "../screens/ExploreScreen";
import RewardsScreen from "../screens/RewardsScreen";
import PremiumScreen from "../screens/PremiumScreen";
import UniversityGuide from "../screens/UniversityGuide";
import UniversityGuideDetail from "../screens/UniversityGuideDetail";
import UpdateProfile from "../screens/UpdateProfile";
import ViewDetailsScreen from "../screens/View/ViewDetails";
import Favourite from "../screens/Favourite";
import PrivacyPolicy from "../screens/PrivacyPolicy";
import ActivationMessage from "../screens/ActivationMessage/ActivationMessage";
export const RootStack = createStackNavigator(
  {
    splashScreen: {
      screen: Splash
    },
    loginScreen: {
      screen: Login
    },
    Home: {
      screen: HomeScreen
    },

    Privacypolicy: {
      screen: PrivacyPolicy
    },
    ViewComponent: {
      screen: ViewDetailsScreen
    },

    instituteScreen: {
      screen: Institute
    },

    UpdateProfileScreen: {
      screen: UpdateProfile
    },
    verificationScreen: {
      screen: Verification
    },

    signupScreen: {
      screen: SignUp
    },

    forgetPassword: {
      screen: ForgetPassword
    },

    RevealCodeScreen: {
      screen: RevealCode
    },

    exploreScreen: {
      screen: Explore
    },

    listItemScreen: {
      screen: ListItem
    },

    checkingScreen: {
      screen: PickerWithIcon
    },
    InviteFriends: {
      screen: InviteFriends
    },
    RewardsScreen: {
      screen: RewardsScreen
    },

    TopicPostScreen: {
      screen: TopicPost
    },
    DiscussionScreen: {
      screen: DiscussionForum
    },
    AddForumScreen: {
      screen: AddForum
    },
    Premium: {
      screen: PremiumScreen
    },
    guide: {
      screen: UniversityGuide
    },

    guideDetail: {
      screen: UniversityGuideDetail
    },
    favouriteScreen: {
      screen: Favourite
    },
    ActivationMessage
  },
  {
    headerMode: "none"
  }
);

// export const Tabs = createMaterialTopTabNavigator(
//   {
//     Food: {
//       screen: FoodScreen,
//       navigationOptions: {
//         tabBarLabel: "Food"
// tabBarIcon: ({ tintColor }) => (
//   <MaterialIcons
//     name="account-circle"
//     size={26}
//     style={{ color: tintColor }}
//   />
// )
//   }
// },
// Retail: {
//   screen: RetailScreen,
//   navigationOptions: {
//     tabBarLabel: "Retail"
// tabBarIcon: ({ tintColor }) => (
//   <Ionicons name="ios-time" size={26} style={{ color: tintColor }} />
// )
//   }
// },

// Beauty: {
//   screen: BeautyScreen,
//   navigationOptions: {
//     tabBarLabel: "Beauty & Fitness"
// tabBarIcon: ({ tintColor }) => (
//   <Ionicons name="ios-time" size={26} style={{ color: tintColor }} />
//     // )
//   }
// },

// Entertainment: {
//   screen: EntertaimentScreen,
//   navigationOptions: {
//     tabBarLabel: "Entertainment & Services"
// tabBarIcon: ({ tintColor }) => (
//   <Ionicons name="ios-time" size={26} style={{ color: tintColor }} />
// )
//     }
//   }
// },
// {
//headerMode: "none", // I don't want a NavBar at top
// //tabBarPosition: "top", // So your Android tabs go bottom
// tabBarOptions: {
//   activeTintColor: "red", // Color of tab when pressed
//   inactiveTintColor: "#b5b5b5", // Color of tab when not pressed
//   scrollEnabled: true,
//showLabel: Platform.OS !== "android", //No label for Android
// labelStyle: {
//   fontSize: 11
// },
// style: {
//   backgroundColor: "#fff" // Makes Android tab bar white instead of standard blue
//height: Platform.OS === "ios" ? 48 : 50 // I didn't use this in my app, so the numbers may be off.
//       }
//     }
//   }
// );

// export const ListItemTabs = createMaterialTopTabNavigator(
//   {
//     Deals: {
//       screen: DealsScreen,
//       navigationOptions: {
//         tabBarLabel: "Deals"
// tabBarIcon: ({ tintColor }) => (
//   <MaterialIcons
//     name="account-circle"
//     size={26}
//     style={{ color: tintColor }}
//   />
// )
//   }
// },
// Info: {
//   screen: InfoScreen,
//   navigationOptions: {
//     tabBarLabel: "Info"
// tabBarIcon: ({ tintColor }) => (
//   <Ionicons name="ios-time" size={26} style={{ color: tintColor }} />
// )
//   }
// },

// Location: {
//   screen: LocationScreen,
//   navigationOptions: {
//     tabBarLabel: "Location"
// tabBarIcon: ({ tintColor }) => (
//   <Ionicons name="ios-time" size={26} style={{ color: tintColor }} />
// )
//     }
//   }
// },
// {
//headerMode: "none", // I don't want a NavBar at top
//tabBarPosition: "top", // So your Android tabs go bottom
// tabBarOptions: {
//   activeTintColor: "red", // Color of tab when pressed
//   inactiveTintColor: "#b5b5b5",
// Color of tab when not pressed
//showLabel: Platform.OS !== "android", //No label for Android
// labelStyle: {
//   fontSize: 13
// },
// style: {
//   backgroundColor: "#fff" // Makes Android tab bar white instead of standard blue
//         //height: Platform.OS === "ios" ? 48 : 50 // I didn't use this in my app, so the numbers may be off.
//       }
//     }
//   }
// );
