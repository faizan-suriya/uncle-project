import axios from "axios";
import { getToken } from "../utils/auth.util";
import Loader from "../components/loader";

const API_VER = "v1";
// const BASE_URL = `https://studenthub-dev.herokuapp.com/api/${API_VER}`;
const BASE_URL = `http://localhost:3008/api/${API_VER}`;

export async function GetApiRequestHeader(token) {
  // ("request header token==>", token);
  const authToken = token || (await getToken());
  // ("This is a auth token", authToken);
  return {
    Accept: "application/json",
    "Content-Type": "application/json",
    "x-access-token": authToken,
    Authorization: `Bearer ${authToken}`
  };
  if (authToken) {
    this.updateHeaders(authToken);
  }
}

const instance = axios.create({
  baseURL: `${BASE_URL}`,
  timeout: 60000,
  withCredentials: true,
  headers: GetApiRequestHeader()
});

export async function updateHeaders(token) {
  // Alter defaults after instance has been created
  // ("Update Header ==>", token);
  const header = await GetApiRequestHeader(token);
  // ("This is header", header);
  instance.defaults.headers = header;
}

export async function request({ method, url, data, headers }) {
  if (this.loading == undefined) {
    this.loading = Loader.show();
  }
  // ("method", method);
  // ("url", url);
  // ("data", data);
  // ("header", headers);
  // (`Sending ${method} request to ${BASE_URL}`, url);

  var promise = instance[method](url, data);
  // // ("Promise", promise);
  let payload;
  try {
    const response = await promise;

    console.log("request>>>", response);
    // (`Response from ${url}`, response);
    payload = response;
    if (this.loading) {
      Loader.hide(this.loading);
      this.loading = undefined;
    }

    if (headers) {
      return {
        data: payload,
        headers: response.headers
      };
    }
    return payload;
  } catch (error) {
    if (this.loading) {
      Loader.hide(this.loading);
      this.loading = undefined;
    }

    throw error;
  }
}

export async function get(url, params, config) {
  // ("Categories Url===>", url);
  return request({ method: "get", url, data: { params }, ...config });
}

export async function del(url, params, config) {
  return request({ method: "delete", url, data: { params }, ...config });
}

export async function post(url, data, config) {
  // ("Url", url);
  // ("data==>", data);
  // ("Config", config);
  // // ("config", ...config);
  return request({ method: "post", url, data, ...config });
}

export async function put(url, data, config) {
  return request({ method: "put", url, data, ...config });
}

// async function GetApiRequestHeader() {
//   debugger;
//   const authToken = await getToken();
//   return {
//     Accept: "application/json",
//     "Content-Type": "application/json",
//     Authorization: `Bearer ${authToken}`
//   };
// }

// _checkStatus(){

// }

// export function get({ url }) {
//   return axios.get(`${BASE_URL}/${url}`);
// }

// export async function post({ url, body }) {
// const _hToken ='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1YmNmYTUxMThiMjk4NjQ1NWNiYzQ1ZWEiLCJlbWFpbCI6ImFzaWZAdGVzdC5jb20iLCJwYXNzd29yZCI6ImNmYTNkYWIwNmJkMzRlZDZkNTgzNWQ2MTYxY2IzMDQwIiwibW9iaWxlIjoiMTIzNDU3MDAiLCJpYXQiOjE1NDA4OTk0MTB9.ic3kY3NG54kFvTXwvOSvFSSqbDoiNjvqZMQEYyG6zZ4'
//   const token = await getToken();
//   // ("This is my token", token);
//   const headers = {
//     "Content-Type": "application/json",
//     Authorization: `Bearer ${token}`
//   };
//   try {
//     return axios.post(`${BASE_URL}/${url}`, body, { headers });
//   } catch (error) {
//     // (error);
//   }
// }

// export function put({ url, body }) {
//   return axios.put(`${BASE_URL}/${url}`, body);
// }

// export function remove({ url }) {
//   return axios.remove(`${BASE_URL}/${url}`);
// }
