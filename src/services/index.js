import { updateHeaders } from "./httpProvider";
import authServices from "./users";

export default {
  auth: authServices
};

export { updateHeaders };
