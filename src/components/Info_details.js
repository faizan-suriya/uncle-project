import * as React from "react";
import { View, Text, StyleSheet, Dimensions } from "react-native";
import { TabView, TabBar, SceneMap, PagerPan } from "react-native-tab-view";
import StoreList from "../components/listview";
import JsonData from "../../mockupdata/storedata.json";
import { Colors } from "../styles/colors";
import ProfileInformation from "../components/Personal_Info";
import InstituteInformation from "../components/InstituteInfo";

const FirstRoute = props => (
  <View style={{ flex: 1 }}>
    <ProfileInformation />
  </View>
);

const SecondRoute = props => (
  <View style={{ flex: 1 }}>
    <InstituteInformation />
  </View>
);

export default class TabViewDetail extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    index: 0,
    routes: [
      {
        key: "first",
        title: "Personal Information",
        navigation: this.props.navigation
      },
      {
        key: "second",
        title: "Institute Information",
        navigation: this.props.navigation
      }
    ]
  };

  __handleIndexChange = index => this.setState({ index });

  __renderTabBar = props => (
    <TabBar
      {...props}
      style={{ backgroundColor: Colors.cardColor, borderBottomColor: "blue" }}
      // tabStyle={{Tab:"blue"}}
      labelStyle={{
        color: Colors.white,
        fontSize: 11,
        fontWeight: "bold",
        textAlign: "center"
      }}
      // tabStyle={{ borderColor: "red", borderWidth: 1, padding: 0 }}
      indicatorStyle={{
        backgroundColor: "white"
      }}
    />
  );
  __renderPager = props => <PagerPan {...props} />;

  _renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute
  });

  render() {
    return (
      <TabView
        indicatorStyle={{ color: "blue" }}
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this.__renderTabBar}
        renderPager={this.__renderPager}
        onIndexChange={this.__handleIndexChange}
        initialLayout={{
          width: Dimensions.get("window").width
          // tabOptions();
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1
  }
});
