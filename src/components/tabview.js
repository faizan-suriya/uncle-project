import * as React from "react";
import { View, Text, StyleSheet, Dimensions } from "react-native";
import { TabView, TabBar, SceneMap } from "react-native-tab-view";
import StoreList from "../components/listview";
import JsonData from "../../mockupdata/storedata.json";

const FirstRoute = props => (
  <View>
    <StoreList {...props} data={JsonData.food} />
  </View>
);

const SecondRoute = props => (
  <View>
    <StoreList {...props} data={JsonData.retail} />
  </View>
);

const ThirdRoute = props => (
  <View>
    <StoreList {...props} data={JsonData.food} />
  </View>
);

export default class TabViewExample extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    index: 0,
    routes: [
      {
        key: "first",
        title: "How it works",
        navigation: this.props.navigation
      },
      {
        key: "second",
        title: "Invite friends",
        navigation: this.props.navigation
      },
      { key: "third", title: "My rewards", navigation: this.props.navigation }
    ]
  };

  _handleIndexChange = index => this.setState({ index });

  _renderTabBar = props => <TabBar {...props} scrollEnabled={false} />;

  _renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    third: ThirdRoute
  });

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
        initialLayout={{
          width: Dimensions.get("window").width
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1
  }
});
