import React from "react";
import {
  Container,
  View,
  Card,
  CardItem,
  Left,
  Right,
  Button,
  Icon,
  Item,
  Input,
  Text,
  Thumbnail,
  RefreshControl
} from "native-base";
import PostComment from "../../components/comments/comments";
import CommentInput from "../../components/comments/input";

import { StyleSheet, ScrollView, AsyncStorage } from "react-native";
import Icons from "react-native-vector-icons/FontAwesome";
import Logo from "../../components/logo";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import Snackbar from "react-native-snackbar";
import MainBackground from "../../components/MainBackground";
import services from "../../services";
import * as authUtil from "../../utils/auth.util";
import { get } from "lodash";
import { emailConstraint, passwordConstraint } from "../../utils/constraints";
var validate = require("validate.js");
import { String } from "../../utils/strings";
import { Colors } from "../../styles/colors";
import { Assets } from "../../../assets";
import Navbar from "../../components/navbar";

class TopicPost extends React.Component {
  constructor() {
    super();
    this.state = {
      comments: [],
      refreshing: true
    };
  }

  componentDidMount() {
    this._fetchCommnets();
  }

  _fetchCommnets = async postId => {
    try {
      const getData = await services.auth.getCommentByPost(
        this.props.navigation.state.params.post._id
      );
      // ("This is the comment Id", getData);
      this.setState({ comments: getData.data });
    } catch (error) {
      // (error);
    }
  };


  

  render() {
    const { comments } = this.state;
    var left = (
      <Left>
        <Button onPress={() => this.props.navigation.pop()} transparent>
          <Icon name="ios-arrow-back" style={{ color: Colors.cardColor }} />
        </Button>
      </Left>
    );
    return (
      <Container style={{ opacity: 0.9 }}>
        <Navbar left={left} title="Topic Post" />
        <ScrollView>
          <View style={styles.container}>
            <View style={{ flexDirection: "column" }}>
              <View style={styles.itemBlock}>
                <Thumbnail
                  square
                  source={{
                    uri: this.props.navigation.state.params.post.thumb
                  }}
                  style={styles.itemImage}
                />
                <View style={styles.itemMeta}>
                  <Text style={styles.itemName}>
                    {this.props.navigation.state.params.post.text}
                  </Text>
                  <Text style={styles.time}>
                    {this.props.navigation.state.params.post.Time}
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.description}>
              <Text style={styles.summary}>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </Text>
            </View>
            <View>
              <Thumbnail
                square
                source={{
                  uri: this.props.navigation.state.params.post.thumb
                }}
                style={styles.mainImage}
              />
            </View>
            <View style={styles.itemBlock}>
              <Button
                onPress={() => alert("This is the notification")}
                transparent
              >
                <Icons name="thumbs-up" style={styles.iconStyle} />
                <Text>
                  {this.props.navigation.state.params.post.totalLikes}
                </Text>
              </Button>
              <Button
                onPress={() => alert("This is the notification")}
                transparent
              >
                <Icons name="comment" style={styles.iconStyle} />
                <Text>
                  {this.props.navigation.state.params.post.totalComments}
                </Text>
              </Button>
            </View>
            <View style={styles.commentcontainer}>
              <ScrollView>
                {comments.map((comment, index) => (
                  <PostComment comment={comment} key={index} />
                ))}
              </ScrollView>

              <CommentInput onSubmit={this.submitComment} />
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15
  },
  itemBlock: {
    flexDirection: "row",
    paddingBottom: 5
  },
  itemImage: {
    width: 80,
    height: 80,
    borderRadius: 20
  },
  mainImage: {
    width: "100%",
    height: 220
  },
  itemMeta: {
    marginLeft: 10,
    justifyContent: "center",
    flex: 1
  },
  iconStyle: {
    color: Colors.cardColor
  },
  itemName: {
    fontSize: 20,
    fontWeight: "bold",
    color: Colors.cardColor
  },
  time: {
    fontSize: 12,
    color: Colors.darkGrey
  },
  summary: {
    fontSize: 15,
    color: Colors.black,
    marginBottom: 15
  },
  separator: {
    height: 0.5,
    width: "80%",
    alignSelf: "center",
    backgroundColor: "#555"
  },
  header: {
    padding: 10
  },
  headerText: {
    fontSize: 30,
    fontWeight: "900"
  },

  commentcontainer: {
    flex: 1,
    backgroundColor: "#FFF",
    paddingTop: 20
  }
});

export default TopicPost;
