import React, { Component } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import moment from "moment";

export default class PostComment extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    // Pull comment object out of props
    const { comment } = this.props;
    // ("This is comment page shows", comment);
    // Pull data needed to display a comment out of comment object
    // const { content, created, user } = comment;
    // Pull user name and avatar out of user object
    // const { name, avatar } = user;
    return (
      <View style={styles.container}>
        {/* <View style={styles.avatarContainer}>
          {avatar && (
            <Image
              resizeMode="contain"
              style={styles.avatar}
              source={{ uri: avatar }}
            />
          )}
        </View> */}
        <View style={styles.contentContainer}>
          <Text>
            {/* <Text style={[styles.text, styles.name]}>{name}</Text> */}

            <Text style={styles.text}>{comment.text}</Text>
          </Text>
          <Text style={[styles.text, styles.created]}>
            {moment(comment.date).fromNow()}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  avatarContainer: {
    alignItems: "center",
    marginLeft: 5,
    paddingTop: 10,
    width: 40
  },
  contentContainer: {
    flex: 1,
    borderBottomWidth: 1,
    borderColor: "#EEE",
   // padding: 5
  },
  avatar: {
    borderWidth: 1,
    borderColor: "#EEE",
    borderRadius: 13,
    width: 26,
    height: 26
  },
  text: {
    color: "#000",
    fontFamily: "Avenir",
    fontSize: 15
  },
  name: {
    fontWeight: "bold"
  },
  created: {
    color: "#BBB"
  }
});
