import React from "react";
import { StyleSheet, ImageBackground } from "react-native";
import { Assets } from "../../assets";

const SplashBackground = ({ style = {}, ...props }) => {
  return (
    <ImageBackground
      resizeMode="cover"
      style={[StyleSheet.absoluteFill, style]}
      source={Assets.splashBackground}
      {...props}
    />
  );
};

export default SplashBackground;
