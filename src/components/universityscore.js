import React, { Component } from "react";
import { StyleSheet, FlatList } from "react-native";
import StarRating from "react-native-star-rating";
import { View, Text, Container } from "native-base";
import { Colors } from "../styles/colors";

class UniversityScore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      universityScore: [
        {
          id: 1,
          score_title: "Entry Standards",
          star_rating: 2.5,
          number_rating: 186
        },
        {
          id: 2,
          score_title: "Graduate Prospects",
          star_rating: 4,
          number_rating: 186
        },
        {
          id: 3,
          score_title: "Student Satisfaction",
          star_rating: 3.9,
          number_rating: 186
        },
        {
          id: 4,
          score_title: "Research Quality",
          star_rating: 2.8,
          number_rating: 186
        },
        {
          id: 5,
          score_title: "Academic Services Spend",
          star_rating: 3.6,
          number_rating: 186
        },
        {
          id: 6,
          score_title: "Degree Completion",
          star_rating: 4.8,
          number_rating: 186
        },
        {
          id: 7,
          score_title: "Student-Staff Ratio",
          star_rating: 1.6,
          number_rating: 186
        },
        {
          id: 8,
          score_title: "Good Honours",
          star_rating: 3.6,
          number_rating: 186
        },
        {
          id: 9,
          score_title: "Facilities Spend",
          star_rating: 5,
          number_rating: 186
        }
      ],
      starCount: 4
    };
  }

  componentDidMount() {
    const getData = this.props.score;
    // ("This is the score", getData);
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }

  renderItem(data) {
    let { item, index } = data;
    return (
      <View style={styles.container}>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <View style={{ flex: 1 }}>
            <Text style={{ fontSize: 14, fontWeight: "bold" }}>
              {item.name}
            </Text>
          </View>
          <View style={{ flexDirection: "row", paddingRight: 10 }}>
            <View style={{ paddingRight: 10 }}>
              <Text style={{ fontSize: 12 }}>{item.rating}</Text>
            </View>
            <View>
              <StarRating
                disabled={false}
                maxStars={5}
                starSize={15}
                starStyle={{ paddingRight: 5 }}
                fullStarColor="#FFC600"
                rating={item.rating}
                selectedStar={rating => this.onStarRatingPress(rating)}
              />
            </View>
          </View>
        </View>
      </View>
    );
  }

  render() {
    const getData = this.props.route.score;
    console.log("This is the score data", getData);
    return (
      <View style={styles.container}>
        <FlatList
          // keyExtractor={this._keyExtractor}
          keyExtractor={(item, index) => index.toString()}
          // keyExtractor={(item, index) => item.id}
          data={getData}
          renderItem={this.renderItem.bind(this)}
          // ItemSeparatorComponent={this.renderSeparator.bind(this)}
          // ListHeaderComponent={this.renderHeader}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    //flex: 1,
    // backgroundColor: "red",
    // marginBottom: 50,
    marginTop: 5
  },
  title: {
    fontSize: 15
  },
  circles: {
    flexDirection: "row",
    alignItems: "center"
  },
  progress: {
    marginTop: 5,
    marginBottom: 5
  },
  percent: {
    fontSize: 12
  },
  progressview: {
    marginBottom: 10
  }
});

export default UniversityScore;
