import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  ListView,
  Image,
  TouchableOpacity,
  StyleSheet,
  ScrollView
} from "react-native";

// import { List, ListItem, SearchBar } from "react-native-elements";

class StoreList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // loading: false,
      // data: [],
      // page: 1,
      // seed: 1,
      // error: null,
      // refreshing: false
      datasource: props.data
    };
  }

  // componentDidMount() {
  //   this.makeRemoteRequest();
  // }

  // makeRemoteRequest = () => {
  //   const { page, seed } = this.state;
  //   const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
  //   this.setState({ loading: true });

  //   fetch(url)
  //     .then(res => res.json())
  //     .then(res => {
  //       this.setState({
  //         data: page === 1 ? res.results : [...this.state.data, ...res.results],
  //         error: res.error || null,
  //         loading: false,
  //         refreshing: false
  //       });
  //     })
  //     .catch(error => {
  //       this.setState({ error, loading: false });
  //     });
  // };

  // handleRefresh = () => {
  //   this.setState(
  //     {
  //       page: 1,
  //       seed: this.state.seed + 1,
  //       refreshing: true
  //     },
  //     () => {
  //       this.makeRemoteRequest();
  //     }
  //   );
  // };

  // handleLoadMore = () => {
  //   this.setState(
  //     {
  //       page: this.state.page + 1
  //     },
  //     () => {
  //       this.makeRemoteRequest();
  //     }
  //   );
  // };

  // renderSeparator = () => {
  //   return (
  //     <View
  //       style={{
  //         height: 1,
  //         width: "86%",
  //         backgroundColor: "#CED0CE",
  //         marginLeft: "14%"
  //       }}
  //     />
  //   );
  // };

  // renderHeader = () => {
  //   return <SearchBar placeholder="Type Here..." lightTheme round />;
  // };

  // renderFooter = () => {
  //   if (!this.state.loading) return null;

  //   return (
  //     <View
  //       style={{
  //         paddingVertical: 20,
  //         borderTopWidth: 1,
  //         borderColor: "#CED0CE"
  //       }}
  //     >
  //       <ActivityIndicator animating size="large" />
  //     </View>
  //   );
  // };

  // _onPress = () => {
  //   alert("You clicked the Item");
  //   // ("hello", this.props);
  //   this.props.route.navigation.navigate("listItemScreen", {
  //     id: item.id,
  //     title: item.title
  //   });
  // };

  // _handleLoadMore = () => {
  //   this.setState({ page: this.state.page + 1, }, () => {
  //     this.makeRemoteRequest();
  //   });
  // };

  _renderItem = ({ item }) => {
    const { navigate } = this.props.route.navigation;
    return (
      <TouchableOpacity
        style={{ flex: 1, flexDirection: "row", marginBottom: 3 }}
        onPress={() =>
          navigate("listItemScreen", {
            title: item.shop_title,
            phone: item.phone_number,
            info : item.information
          })
        }
      >
        <View style={{ flex: 1, flexDirection: "row", marginBottom: 3 }}>
          <Image
            style={{ width: 100, height: 100, margin: 5 }}
            source={{ uri: item.image }}
          />
          <View style={{ flex: 1, justifyContent: "center" }}>
            <Text style={{ fontSize: 18, marginBottom: 15 }}>
              {item.shop_title}
            </Text>
            <Text>{item.distance}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  _renderSeparator = () => {
    return (
      <View style={{ height: 1, width: "100%", backgroundColor: "#b5b5b5" }} />
    );
  };

  // componentDidMount() {
  //   const url = "../../assets/bookdata.json";
  //   fetch(url)
  //     .then(response => response.json())
  //     .then(res => {
  //       alert(res.book_array);
  //       this.setState({
  //         datasource: res.book_array
  //       });
  //     })
  //     .catch(error => {
  //       // (error);
  //     });
  // }
  render() {
    return (
      // <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
      //   <FlatList
      //     data={this.state.data}
      //     renderItem={({ item }) => (
      //       <ListItem
      //         roundAvatar
      //         title={`${item.name.first} ${item.name.last}`}
      //         subtitle={item.email}
      //         avatar={{ uri: item.picture.thumbnail }}
      //         containerStyle={{ borderBottomWidth: 0 }}
      //       />
      //     )}
      //     keyExtractor={item => item.email}
      //     ItemSeparatorComponent={this.renderSeparator}
      //     ListHeaderComponent={this.renderHeader}
      //     ListFooterComponent={this.renderFooter}
      //     onRefresh={this.handleRefresh}
      //     refreshing={this.state.refreshing}
      //     onEndReached={this.handleLoadMore}
      //     onEndReachedThreshold={50}
      //   />
      // </List>
      <View style={styles.container}>
        <FlatList
          style={{
            width: "100%",
            borderWidth: 1,
            borderColor: "#e0e1e2",
            marginTop: 10
          }}
          data={this.state.datasource}
          renderItem={this._renderItem.bind(this)}
          ItemSeparatorComponent={this._renderSeparator}
          keyExtractor={(item, index) => item.id}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({});

export default StoreList;
