import React, { Component } from "react";
import { Text, View, Image, StyleSheet, ScrollView } from "react-native";
import { Left, Right, Icon, Button } from "native-base";
import { ListItemTabs } from "../routes";
import InnerTabView from "./innertabview";
import Navbar from "./navbar";

const _banner = require("../../assets/slideimage1.jpeg");
const _logo = require("../../assets/burgerlab.png");

export default class ListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let left = (
      <Left style={{ flex: 1 }}>
        <Button
          onPress={() => this.props.navigation.navigate("Home")}
          transparent
        >
          <Icon name="ios-arrow-back" />
        </Button>
      </Left>
    );

    const { navigation } = this.props;
    // const paramater = navigation.getParam;
    // // (paramater);
    return (
      <View style={styles.container}>
        <Navbar left={left} title={navigation.state.params.title} />
        <Image source={_banner} />
        <View
          style={{
            paddingTop: 10,
            paddingBottom: 10,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "#f7f7f7"
          }}
        >
          <Text style={{ fontWeight: "bold" }}>
            Call# {navigation.state.params.phone}
          </Text>
        </View>
        <View style={styles.tabscontainer}>
          <InnerTabView storedata={navigation.state.params.info} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  tabscontainer: {
    flex: 1
  }
});
