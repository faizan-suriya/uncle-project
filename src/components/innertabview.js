import * as React from "react";
import { View, StyleSheet, Dimensions, Text } from "react-native";
import { TabView, TabBar, SceneMap } from "react-native-tab-view";
import DealListView from "./deallistview";
import StoreInfo from "./info";
import Map from "./map";
import LocationScreen from "../screens/LocationScreen";
import StoreList from "../components/listview";
import JsonData from "../../mockupdata/storedata.json";

const FirstRoute = props => (
  <View>
    <DealListView />
  </View>
);

const SecondRoute = props => (
  <View
    style={{
      flex: 1
    }}
  >
    <StoreInfo />
  </View>
);

const ThirdRoute = props => (
  <View
    style={{
      flex: 1,
      justifyContent: "flex-end",
      alignItems: "center"
    }}
  >
    <Map />
  </View>
);

export default class InnerTabView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      routes: [
        { key: "first", title: "Deals" },
        {
          key: "second",
          title: "Info"
        },
        { key: "third", title: "Location" }
      ]
    };
  }

  _handleIndexChange = index => this.setState({ index });

  _renderTabBar = props => <TabBar {...props} />;

  // _renderScene = SceneMap({
  //   first: FirstRoute,
  //   second: SecondRoute,
  //   third: ThirdRoute
  // });

  _renderScene = ({ route }) => {
    switch (route.key) {
      case "first":
        return <FirstRoute />;
      case "second":
        return <SecondRoute />;
      case "third":
        return <ThirdRoute />;
      default:
        return null;
    }
  };

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
        initialLayout={{
          width: Dimensions.get("window").width
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1
  }
});
