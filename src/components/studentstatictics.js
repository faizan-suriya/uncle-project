import React, { Component } from "react";

import { StyleSheet, Text, View } from "react-native";

import * as Progress from "react-native-progress";

export default class StudentStatictics extends Component {
  constructor(props) {
    super(props);

    this.state = {
      progress: 0,
      indeterminate: true
    };
  }

  // componentDidMount() {
  //   this.animate();
  // }

  // animate() {
  //   let progress = 0;
  //   this.setState({ progress });
  //   setTimeout(() => {
  //     this.setState({ indeterminate: false });
  //     setInterval(() => {
  //       progress += Math.random() / 5;
  //       if (progress > 1) {
  //         progress = 1;
  //       }
  //       this.setState({ progress });
  //     }, 500);
  //   }, 1500);
  // }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.progressview}>
          <Text style={styles.title}>Level of study</Text>
          <Progress.Bar
            progress={0.5}
            width={null}
            borderWidth={0}
            color="orange"            unfilledColor="black"
            style={styles.progress}
          />
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ flex: 1 }}>
              <Text style={styles.percent}>Undergraduate 60%</Text>
            </View>
            <View>
              <Text style={styles.percent}>Postgraduate 40%</Text>
            </View>
          </View>
        </View>
        <View style={styles.progressview}>
          <Text style={styles.title}>Admission Rate</Text>
          <Progress.Bar
            progress={0.7}
            width={null}
            borderWidth={0}
            color="orange"
            unfilledColor="black"
            style={styles.progress}
          />
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ flex: 1 }}>
              <Text style={styles.percent}>Morning Time 60%</Text>
            </View>
            <View>
              <Text style={styles.percent}>Evening Time 40%</Text>
            </View>
          </View>
        </View>
        <View style={styles.progressview}>
          <Text style={styles.title}>Where students come from </Text>
          <Progress.Bar
            progress={0.3}
            width={null}
            borderWidth={0}
            color="orange"
            unfilledColor="black"
            style={styles.progress}
          />
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ flex: 1 }}>
              <Text style={styles.percent}>Karachi 60%</Text>
            </View>
            <View>
              <Text style={styles.percent}>Others 40%</Text>
            </View>
          </View>
        </View>
        <View style={styles.progressview}>
          <Text style={styles.title}>Student Gender</Text>
          <Progress.Bar
            progress={0.3}
            width={null}
            borderWidth={0}
            color="orange"
            unfilledColor="black"
            style={styles.progress}
          />
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ flex: 1 }}>
              <Text style={styles.percent}>Male 60%</Text>
            </View>
            <View>
              <Text style={styles.percent}>Female 40%</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
     //flex: 1,
    //  backgroundColor: "green",
   // marginBottom: 50,
    marginTop:5
  },
  title: {
    fontSize: 15
  },
  circles: {
    flexDirection: "row",
    alignItems: "center"
  },
  progress: {
    marginTop: 5,
    marginBottom: 5
  },
  percent: {
    fontSize: 12
  },
  progressview: {
    marginBottom: 10
  }
});
