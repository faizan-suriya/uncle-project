import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  ListView,
  Image,
  TouchableOpacity,
  StyleSheet,
  ScrollView
} from "react-native";

import dealdata from "../../mockupdata/dealdata.json";
const _logo = require("../../assets/burgerlab.png");

// import { List, ListItem, SearchBar } from "react-native-elements";

class DealListView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // loading: false,
      // data: [],
      // page: 1,
      // seed: 1,
      // error: null,
      // refreshing: false
      datasource: dealdata
      // [
      //   {
      //     id: 1,
      //     shop_title: "Burger Lab",
      //     image: "https://i.imgur.com/DwduqUMb.jpg",
      //     distance: "14km"
      //   },
      //   {
      //     id: 2,
      //     shop_title: "The book One title",
      //     image: "https://i.imgur.com/DwduqUMb.jpg",
      //     distance: "32km"
      //   },
      //   {
      //     id: 3,
      //     shop_title: "The book One title",
      //     image: "https://i.imgur.com/DwduqUMb.jpg",
      //     distance: "7km"
      //   },
      //   {
      //     id: 4,
      //     shop_title: "The book One title",
      //     image: "https://i.imgur.com/DwduqUMb.jpg",
      //     distance: "19km"
      //   },
      //   {
      //     id: 5,
      //     shop_title: "The book One title",
      //     image: "https://i.imgur.com/DwduqUMb.jpg",
      //     distance: "54km"
      //   },
      //   {
      //     id: 6,
      //     shop_title: "The book One title",
      //     image: "https://i.imgur.com/DwduqUMb.jpg",
      //     distance: "7km"
      //   },
      //   {
      //     id: 7,
      //     shop_title: "The book One title",
      //     image: "https://i.imgur.com/DwduqUMb.jpg",
      //     distance: "19km"
      //   },
      //   {
      //     id: 8,
      //     shop_title: "The book One title",
      //     image: "https://i.imgur.com/DwduqUMb.jpg",
      //     distance: "19km"
      //   },
      //   {
      //     id: 9,
      //     shop_title: "The book One title",
      //     image: "https://i.imgur.com/DwduqUMb.jpg",
      //     distance: "7km"
      //   },
      //   {
      //     id: 10,
      //     shop_title: "Ambala Foods",
      //     image: "https://i.imgur.com/DwduqUMb.jpg",
      //     distance: "19km"
      //   },
      //   {
      //     id: 11,
      //     shop_title: "Ambala Foods",
      //     image: "https://i.imgur.com/DwduqUMb.jpg",
      //     distance: "19km"
      //   },
      //   {
      //     id: 12,
      //     shop_title: "Ambala Foods",
      //     image: "https://i.imgur.com/DwduqUMb.jpg",
      //     distance: "19km"
      //   },
      //   {
      //     id: 13,
      //     shop_title: "Ambala Foods",
      //     image: "https://i.imgur.com/DwduqUMb.jpg",
      //     distance: "19km"
      //   }
      // ]
    };
  }

  // componentDidMount() {
  //   this.makeRemoteRequest();
  // }

  // makeRemoteRequest = () => {
  //   const { page, seed } = this.state;
  //   const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
  //   this.setState({ loading: true });

  //   fetch(url)
  //     .then(res => res.json())
  //     .then(res => {
  //       this.setState({
  //         data: page === 1 ? res.results : [...this.state.data, ...res.results],
  //         error: res.error || null,
  //         loading: false,
  //         refreshing: false
  //       });
  //     })
  //     .catch(error => {
  //       this.setState({ error, loading: false });
  //     });
  // };

  // handleRefresh = () => {
  //   this.setState(
  //     {
  //       page: 1,
  //       seed: this.state.seed + 1,
  //       refreshing: true
  //     },
  //     () => {
  //       this.makeRemoteRequest();
  //     }
  //   );
  // };

  // handleLoadMore = () => {
  //   this.setState(
  //     {
  //       page: this.state.page + 1
  //     },
  //     () => {
  //       this.makeRemoteRequest();
  //     }
  //   );
  // };

  // renderSeparator = () => {
  //   return (
  //     <View
  //       style={{
  //         height: 1,
  //         width: "86%",
  //         backgroundColor: "#CED0CE",
  //         marginLeft: "14%"
  //       }}
  //     />
  //   );
  // };

  // renderHeader = () => {
  //   return <SearchBar placeholder="Type Here..." lightTheme round />;
  // };

  // renderFooter = () => {
  //   if (!this.state.loading) return null;

  //   return (
  //     <View
  //       style={{
  //         paddingVertical: 20,
  //         borderTopWidth: 1,
  //         borderColor: "#CED0CE"
  //       }}
  //     >
  //       <ActivityIndicator animating size="large" />
  //     </View>
  //   );
  // };

  _onPress = () => {
    alert("You clicked the Item");
  };

  // _handleLoadMore = () => {
  //   this.setState({ page: this.state.page + 1, }, () => {
  //     this.makeRemoteRequest();
  //   });
  // };

  _renderItem = ({ item, i }) => {
    return (
      <TouchableOpacity
        key={i}
        style={{ flex: 1, flexDirection: "row", marginBottom: 3 }}
        onPress={this._onPress.bind(this)}
      >
        <View style={{ flex: 1, flexDirection: "row", marginBottom: 3 }}>
          <Image
            style={{ width: 100, height: 100, margin: 5 }}
            source={{ uri: item.deal_image }}
          />
          <View style={{ flex: 1, justifyContent: "center" }}>
            <Text style={{ fontSize: 18, marginBottom: 15 }}>
              {item.deal_title}
            </Text>
            <Text>{item.deal_description}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  _renderSeparator = () => {
    return (
      <View style={{ height: 1, width: "100%", backgroundColor: "#b5b5b5" }} />
    );
  };

  // componentDidMount() {
  //   const url = "../../assets/bookdata.json";
  //   fetch(url)
  //     .then(response => response.json())
  //     .then(res => {
  //       alert(res.book_array);
  //       this.setState({
  //         datasource: res.book_array
  //       });
  //     })
  //     .catch(error => {
  //       // (error);
  //     });
  // }
  render() {
    return (
      // <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
      //   <FlatList
      //     data={this.state.data}
      //     renderItem={({ item }) => (
      //       <ListItem
      //         roundAvatar
      //         title={`${item.name.first} ${item.name.last}`}
      //         subtitle={item.email}
      //         avatar={{ uri: item.picture.thumbnail }}
      //         containerStyle={{ borderBottomWidth: 0 }}
      //       />
      //     )}
      //     keyExtractor={item => item.email}
      //     ItemSeparatorComponent={this.renderSeparator}
      //     ListHeaderComponent={this.renderHeader}
      //     ListFooterComponent={this.renderFooter}
      //     onRefresh={this.handleRefresh}
      //     refreshing={this.state.refreshing}
      //     onEndReached={this.handleLoadMore}
      //     onEndReachedThreshold={50}
      //   />
      // </List>
      <View>
        <FlatList
          style={{
            width: "100%",
            borderWidth: 1,
            borderColor: "#e0e1e2",
            marginTop: 10
          }}
          data={this.state.datasource}
          renderItem={this._renderItem.bind(this)}
          ItemSeparatorComponent={this._renderSeparator}
          keyExtractor={(item, index) => item.id}
        />
      </View>
    );
  }
}

export default DealListView;
