import Loading from "react-native-loader-overlay";
import { Colors } from "../styles/colors";
class Loader {
  show() {
    return Loading.show({
      color: Colors.maroonColor,
      size: 15,
      overlayColor: "rgba(0,0,0,0.5)",
      closeOnTouch: true,
      loadingType: "Bubbles" // 'Bubbles', 'DoubleBounce', 'Bars', 'Pulse', 'Spinner'
    });
  }

  hide(loader) {
    Loading.hide(loader);
  }
}

export default new Loader();
