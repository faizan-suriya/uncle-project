import React, { Component } from "react";
import { StyleSheet, TouchableOpacity, AsyncStorage } from "react-native";
import {
  Container,
  View,
  Text,
  Content,
  Left,
  right,
  Item,
  Icon,
  Input,
  Button
} from "native-base";
import { Colors } from "../styles/colors";
import Navbar from "./navbar";
import { String } from "../utils/strings";
import * as authUtil from "../utils/user.util";
import * as userUtil from "../utils/user.util";
import services from "../services";
import Snackbar from "react-native-snackbar";
import DateTimePicker from "react-native-modal-datetime-picker";
var dateFormat = require("dateformat");

class ProfileInformation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      mobile: ""
    };
  }
  async componentDidMount() {
    const getData = await userUtil.getUserInfo();
    this.setState({
      firstName: getData.firstName,
      lastName: getData.lastName,
      email: getData.email,
      mobile: getData.mobile,
      date: getData.dob
    });
  }

  _updateProfile = async () => {
    const params = {
      firstName: this.state.firstName,
      lastName: this.state.lastName
    };
    if (this.state.date) {
      params.dob = this.state.date;
    }
    try {
      const { data } = await services.auth.updateProfile(params);
      await userUtil.setUserInfo(data);
      Snackbar.show({
        title: "Profile Update Successfully",
        duration: Snackbar.LENGTH_LONG
      });
    } catch (error) {
      // (error);
    }
  };

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = date => {
    console.log("A date has been picked: ", date);
    this.setState({ date: dateFormat(date, "mm/dd/yyyy") });
    this._hideDateTimePicker();
  };

  render() {
    var left = (
      <Left>
        <Button onPress={() => this.props.navigation.pop()} transparent>
          <Icon name="ios-arrow-back" style={{ color: Colors.cardColor }} />
        </Button>
      </Left>
    );

    return (
      <Container style={{ backgroundColor: Colors.white }}>
        <Content>
          {/* <Navbar left={left} title="UpdateProfile" /> */}
          {/* <View style={{ backgroundColor: "pink" }}>
            <TabViewDetail />
            </View> */}
          <View style={styles.mainContainer}>
            <View style={styles.bottomcontainer}>
              <Item style={styles.itemStyle}>
                <Icon active name="ios-person" style={styles.icons} />
                <Input
                  style={styles.input}
                  placeholder={String.firstName}
                  value={this.state.firstName}
                  onChangeText={text => this.setState({ firstName: text })}
                  placeholderTextColor={Colors.withPlaceholderColor}
                />
              </Item>
              <Item style={styles.itemStyle}>
                <Icon active name="ios-person" style={styles.icons} />
                <Input
                  style={styles.input}
                  value={this.state.lastName}
                  placeholder={String.lastName}
                  onChangeText={text => this.setState({ lastName: text })}
                  placeholderTextColor={Colors.withPlaceholderColor}
                />
              </Item>
              <Item style={styles.itemStyle}>
                <Icon active name="ios-mail" style={styles.icons} />
                {/* <Text>{this.state.fieldData.email}</Text> */}
                <Input
                  style={styles.input}
                  value={this.state.email}
                  autoCapitalize="none"
                  keyboardType="email-address"
                  disabled={true}
                />
              </Item>
              <Item style={styles.itemStyle}>
                <Icon active name="ios-call" style={styles.icons} />
                <Input
                  style={styles.input}
                  disabled={true}
                  // placeholder={String.phone}
                  // placeholderTextColor={Colors.placeholderColor}
                  value={this.state.mobile}
                  keyboardType="phone-pad"
                  placeholderTextColor={Colors.withPlaceholderColor}
                  refInput={ref => {
                    this.input = ref;
                  }}
                  onChangeText={text => this.setState({ mobile: text.trim() })}
                />
              </Item>
              <Item style={styles.itemStyle}>
                <TouchableOpacity
                  style={{ flex: 1 }}
                  onPress={this._showDateTimePicker}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      flex: 1,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Icon active name="ios-calendar" style={styles.icons} />
                    <Text
                      style={{
                        marginLeft: 12,
                        color: this.state.date ? "black" : "#D3D3D3",
                        fontSize: 14,
                        height: "100%",
                        lineHeight: 40,
                        flex: 1
                      }}
                    >
                      {this.state.date || "Your date of birth (Optional)"}
                    </Text>
                  </View>
                </TouchableOpacity>
              </Item>

              <View
                style={{
                  alignSelf: "stretch"
                }}
              >
                <TouchableOpacity
                  onPress={this._updateProfile}
                  style={styles.button}
                >
                  <Text style={styles.buttonText} uppercase={false}>
                    {String.updateInfo}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <DateTimePicker
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this._handleDatePicked}
            onCancel={this._hideDateTimePicker}
            maximumDate={new Date()}
            mode="date"
          />
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: "center"
  },
  logocontainer: {
    marginBottom: 30,
    marginTop: 10
  },
  bottomcontainer: {
    flex: 1,
    // borderTopLeftRadius: 25,
    // borderTopRightRadius: 25,
    // marginTop: 10,
    paddingTop: 25,
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 5,
    backgroundColor: Colors.white
  },
  icons: {
    color: Colors.black
  },
  input: {
    color: Colors.black
  },
  itemStyle: {
    marginTop: 20
  },
  button: {
    marginTop: 20,
    alignSelf: "stretch",
    height: 45,
    borderRadius: 10,
    backgroundColor: Colors.buttonBackground,
    alignItems: "center",
    justifyContent: "center"
  },

  buttonText: {
    color: Colors.buttonText,
    fontWeight: "bold",
    fontSize: 16
  }
});
export default ProfileInformation;
