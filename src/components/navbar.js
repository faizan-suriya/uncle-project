/**
 * This is the navbar component
 * example of usage:
 *   var left = (<Left><Button transparent><Icon name='menu' /></Button></Left>);
 *   var right = (<Right><Button transparent><Icon name='menu' /></Button></Right>);
 *   <Navbar left={left} right={right} title="My Navbar" />
 **/

// React native and others libraries imports
import React, { Component } from "react";
import { Header, Body, Text, Title, Left, Right, Icon } from "native-base";

// Our custom files and classes import
import { Colors } from "../styles/colors";

export default class Navbar extends Component {
  render() {
    return (
      <Header
        style={{ backgroundColor: Colors.formBackground }}
        // backgroundColor={Colors.navbarBackgroundColor}
        androidStatusBarColor={Colors.formBackground}
        noShadow={true}
      >
        {this.props.left ? this.props.left : <Left />}

        <Body style={styles.body}>
          <Text adjustsFontSizeToFit={true} style={styles.title}>
            {this.props.title}
          </Text>
        </Body>
        {this.props.right ? this.props.right : <Right />}
      </Header>
    );
  }
}

const styles = {
  body: {
    // flex: 1,
    // marginLeft:60
    // justifyContent: "center",
    // alignItems: "center",
    // backgroundColor: "red"
  },
  title: {
    color: Colors.white,
    fontFamily: "Roboto",
    fontWeight: "bold"
    //justifyContent:"center"
    // backgroundColor : "red",
    // textAlign : "left"
  }
};
