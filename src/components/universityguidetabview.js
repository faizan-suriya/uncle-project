import * as React from "react";
import { View, Text, StyleSheet, Dimensions } from "react-native";
import { TabView, TabBar, SceneMap, PagerPan } from "react-native-tab-view";
import StudentStatictics from "../components/studentstatictics";
import UniversityScore from "../components/universityscore";
import StoreList from "../components/listview";
import JsonData from "../../mockupdata/storedata.json";
import { Colors } from "../styles/colors";

const FirstRoute = props => {
  return (
    <View>
      <UniversityScore {...props} />
    </View>
  );
};

const SecondRoute = props => (
  <View>
    <StudentStatictics />
  </View>
);

export default class UniversityGuideTabView extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    index: 0,
    routes: [
      {
        key: "first",
        title: "University Score",
        navigation: this.props.navigation,
        score: this.props.score
      },
      {
        key: "second",
        title: "Student Statictics",
        navigation: this.props.navigation
      }
    ]
  };

  _handleIndexChange = index => this.setState({ index });

  _renderTabBar = props => (
    <TabBar
      {...props}
      style={{ backgroundColor: "transparent" }}
      labelStyle={{ color: Colors.black, fontSize: 13, fontWeight: "bold" }}
      // tabStyle={{ borderColor: "red", borderWidth: 1, padding: 0 }}
      indicatorStyle={{
        backgroundColor: "black"
      }}
    />
  );
  _renderPager = props => <PagerPan {...props} />;

  _renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute
  });

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        renderPager={this._renderPager}
        onIndexChange={this._handleIndexChange}
        initialLayout={{
          width: Dimensions.get("window").width
          //height:14
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1
  }
});
